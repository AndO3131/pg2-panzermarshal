#  Panzer Marshal Game Assets

This repository contains community  made assets for Panzer Marshal (Open Panzer) Game. You should
visit [Panzer Central](http://panzercentral.com/) as most of the files here were created by the Panzer Central
community. Some of files in this repository might be modified for use with Panzer Marshal.  
 
You can use this repository to: 
1. Edit or create assets (maps, scenarios, campaigns, equipment) using SuitePG2 Panzer General 2 Editor
2. Convert the assets from Panzer General 2 format to Panzer Marshal (Open Panzer) formats using the conversion-tools.

## Installing

Since this repository contains the conversion-tools as a submodule you will need to also download the conversion-tools if
you plan on converting your work to Panzer Marshal formats:

	git clone --recurse-submodules https://bitbucket.org/npavel/pg2-panzermarshal.git

## Adding new stuff

1. Campaigns should go to _panzermarshal/campaigns/[equipment]/MY_CAMPAIGN/_ where _[equipment]_
is the name of the equipment used by the campaign. The name format should be kept as _eqp-[name]_.
**Example**: If your campaign uses an already existing equipment (like adlerkorps) you should copy to
_panzermarshal/campaigns/eqp-adlerkorps/MY_CAMPAIGN/_

2. Equipment (EQUIP97.EQP and other files) should go to  _panzermarshal/eqp/eqp-[name]_ and the Panzer2.dat 
from that equipment (unit icons) should go to _panzermarshal/dat/eqp-[name]_

3. Maps images (png) should go to _panzermarshal/pngmaps/_ if you added a new Campaign make sure you
add the map images used by this campaign. You can use [Map Finder](http://pg2mapfinder.gilestiel.eu/) to download
the map images.

## Editing with Panzer General 2 Tools

### Preparing working folder
1. Run prepare.sh to setup links to existing campaigns (edit this file depending on which EQP file you use). Go to sandbox-<eqp> folder.
2. Run SuitePG2 (wine SuitePG2.exe if not on Windows) and configure "All .MAP files repository" folder to SCENARIO folder in this directory.
3. Load corresponding .eqp file File->Load Equipment and select EQUIP97.EQP file in the root of the sandbox
folder. After this you will be asked to open Panzer2.dat which resides in the same folder.
4. When you open a campaign it will complain about missing maps (eg: MAP#27 not found). Since we aren't using old SHP files the map images
are now png. If you intend to edit the scenario with missing map you can load the corresponding PNG file from MAP/map_27.png) using Map File Editor>Load bitmap either BMP/PNG

### Extracting unit icons
If you add a new equipment for the first time in the game you will have to extract and add icons.

1. Use SHPTool.exe Tools > Dat Tool > Unpack (there is an Unpack button on the toolbar (3 floppy-disk icon)) all icons to disk in a folder you specify.
2. Go to Tools > Shp Tool > Select SHP Folder where you unpacked in step 1
3. Save as BMP > Convert SHP files in a folder to BMP > Select 1x9 format > OK
4. The BMP files should now be extracted to same folder (the UI looks like it's frozen but it works)
5. You can use icons-convert.py script from openpanzer-tools to convert to png

## Converting your work

The details about the conversion tools are available [here](conversion-tools/README.md). If you cloned this repository
you don't necessarily have to edit config/config.py as the defaults should work.
The output of the conversion should be in the root of this repository **conversion-output-date/** folder.


## Testing with Panzer Marshal

You should download an offline version of Panzer Marshal available at [itch.io](https://openpanzer.itch.io/panzermarshal)
and copy the conversion results to your game installation folder **resources/** folder.


## Contribuitors
1. Nicu Pavel
2. Alexander Sayenko


## Copyright

The files are copyrighted to their original authors. The assets provided here can only be used 
with Panzer Marshal and shouldn't be used to create separate products based on Panzer Marshal code.



