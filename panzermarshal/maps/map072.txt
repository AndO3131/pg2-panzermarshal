[MAP:072] [Berlin.map          ]
--------------------------------
(12,01) #Kremmen
(22,01) #Oranienburg
(03,02) #Fehrbellin
(03,03) #Fehrbellin
(26,03) #Wandlitz
(33,03) #Bernau
(34,03) #Bernau
(02,04) #Fehrbellin
(11,04) #Luch
(19,04) #Velten
(41,04) #Protzel
(23,05) #Birkenwerder
(08,06) #Hohen
(19,06) #Hennigsdorf
(20,06) #Hennigsdorf
(22,06) #Birkenwerder
(02,07) #Essin
(18,07) #Schonewalde
(19,07) #Hennigsdorf
(20,07) #Berlin
(21,07) #Birkenwerder
(22,07) #Birkenwerder
(23,07) #Berlin
(27,07) #Zepernick
(28,07) #Zepernick
(32,07) #Krummensee
(37,07) #Strausberg
(38,07) #Strausberg
(05,08) #Nauen
(18,08) #Schonewalde
(19,08) #Berlin
(20,08) #Berlin Mitte
(21,08) #Tegel-Tempelhof Airport
(22,08) #Tegel-Tempelhof Airport
(23,08) #Berlin
(24,08) #Berlin
(25,08) #Berlin
(26,08) #Berlin
(27,08) #Berlin
(28,08) #Berlin
(29,08) #Zepernick
(41,08) #Garzin
(18,09) #Berlin
(19,09) #Berlin
(20,09) #Berlin Mitte
(21,09) #Berlin Mitte
(22,09) #Tegel-Tempelhof Airport
(23,09) #Berlin Mitte
(24,09) #Berlin Mitte
(25,09) #Berlin Mitte
(26,09) #Berlin Mitte
(27,09) #Berlin
(28,09) #Berlin
(29,09) #Berlin
(31,09) #Berlin Airport
(09,10) #Ketzin
(15,10) #Brieselang
(17,10) #Falkensee
(18,10) #Berlin
(19,10) #Berlin Mitte
(20,10) #Berlin Mitte
(21,10) #Berlin Mitte
(22,10) #Berlin Mitte
(23,10) #Berlin Mitte
(24,10) #Berlin Mitte
(25,10) #Berlin Mitte
(26,10) #Berlin Mitte
(27,10) #Berlin
(28,10) #Berlin
(29,10) #Berlin
(30,10) #Berlin Airport
(31,10) #Berlin Airport
(32,10) #Berlin Airport
(16,11) #Falkensee
(17,11) #Falkensee
(18,11) #Berlin Mitte
(19,11) #Berlin Mitte
(20,11) #Berlin Mitte
(22,11) #Berlin Mitte
(23,11) #Berlin Mitte
(24,11) #Berlin Mitte
(25,11) #Berlin Mitte
(26,11) #Berlin Mitte
(27,11) #Berlin
(28,11) #Berlin
(29,11) #Berlin
(30,11) #Berlin
(33,11) #Neuenhagen
(11,12) #Potsdam
(16,12) #Dallgow
(17,12) #Staaken
(18,12) #Staaken
(21,12) #Berlin Mitte
(24,12) #Berlin Mitte
(25,12) #Berlin Mitte
(26,12) #Berlin Mitte
(27,12) #Berlin
(28,12) #Berlin
(29,12) #Berlin
(09,13) #Potsdam
(10,13) #Potsdam
(11,13) #Potsdam
(12,13) #Potsdam
(13,13) #Potsdam
(16,13) #Dallgow
(18,13) #Staaken
(19,13) #Berlin Mitte
(20,13) #Berlin Mitte
(22,13) #Berlin Mitte
(23,13) #Berlin Mitte
(26,13) #Berlin Mitte
(28,13) #Berlin
(29,13) #Berlin
(30,13) #Berlin
(09,14) #Potsdam
(10,14) #Potsdam
(11,14) #Potsdam
(12,14) #Potsdam
(13,14) #Potsdam
(14,14) #Potsdam
(17,14) #Kleinmachnow
(20,14) #Teltow
(21,14) #Berlin
(25,14) #Berlin
(27,14) #Berlin
(30,14) #Berlin
(06,15) #Werder
(08,15) #Potsdam
(10,15) #Potsdam
(11,15) #Potsdam
(12,15) #Potsdam
(14,15) #Potsdam
(15,15) #Potsdam
(17,15) #Kleinmachnow
(18,15) #Kleinmachnow
(19,15) #Teltow
(20,15) #Teltow
(21,15) #Berlin
(22,15) #Berlin
(23,15) #Berlin
(24,15) #Berlin
(25,15) #Berlin
(26,15) #Berlin
(27,15) #Berlin
(28,15) #Berlin
(29,15) #Berlin
(09,16) #Potsdam Airport
(10,16) #Potsdam Airport
(11,16) #Potsdam Airport
(15,16) #Potsdam
(17,16) #Kleinmachnow
(18,16) #Teltow
(19,16) #Teltow
(20,16) #Teltow
(22,16) #Berlin
(23,16) #Berlin
(24,16) #Berlin
(25,16) #Schonefeld
(26,16) #Schonefeld
(27,16) #Schonefeld
(28,16) #Berlin
(29,16) #Berlin
(10,17) #Potsdam Airport
(14,17) #Potsdam
(15,17) #Potsdam
(16,17) #Potsdam
(18,17) #Teltow
(20,17) #Teltow
(22,17) #Mahlowo
(26,17) #Schonefeld
(28,17) #Schonefeld
(03,18) #Burg
(11,18) #Caputh
(19,18) #Teltow
(20,18) #Teltow
(38,18) #Spreenhagen
(09,19) #Caputh
(23,19) #Mahlowo
(01,20) #Buchen
(07,20) #Glindow
(15,20) #Ludwigsfelde
(16,20) #Ludwigsfelde
(22,20) #Blankenfelde
(07,22) #Beelitz
(24,23) #Mittenwalde
(28,23) #Teupitz
(06,24) #Beelitz
(28,24) #Teupitz
(05,25) #Ruck
(10,25) #Trebbin
(18,25) #Zossen
(16,26) #Wunsdorf
