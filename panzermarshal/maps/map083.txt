[MAP:083] [Kharkov.map         ]
--------------------------------
(11,00) #Kharkov River
(05,02) #Dergachi
(06,02) #Dergachi
(07,02) #Dergachi
(10,02) #Kharkov River
(06,03) #Dergachi
(10,03) #Kharkov River
(09,04) #Kharkov River
(20,04) #Khereshonye
(24,04) #Bochkovko
(09,06) #Kharkov River
(11,06) #Khuralevka
(20,06) #Ustinka
(33,07) #Nekhoteyevka
(08,08) #Kharkov River
(21,08) #Vartelevka
(35,08) #Murom
(21,09) #Vartelevka
(22,09) #Vartelevka
(35,09) #Murom
(38,09) #Donets River
(08,10) #Kharkov River
(16,10) #Morokboyets
(33,10) #Pyatuitsa
(07,11) #Kharkov River
(38,11) #Donets River
(11,12) #Borschevoye
(26,12) #Muravlevo
(08,13) #Kharkov River
(11,13) #Borschevoye
(38,13) #Donets River
(40,13) #Krasnoarmeysk
(11,14) #Borschevoye
(21,14) #Puk'yantsy
(37,14) #Donets River
(08,15) #Kharkov River
(10,15) #Borschevoye
(11,15) #Borschevoye
(33,15) #Varvarovka
(07,16) #Kharkov River
(11,16) #Borschevoye
(37,16) #Donets River
(09,17) #Dergachi
(11,17) #Borschevoye
(31,17) #Bayrak
(06,18) #Kharkov River
(11,18) #Borschevoye
(38,18) #Donets River
(07,19) #Kharkov River
(11,19) #Borschevoye
(34,19) #Bolbadka
(07,21) #Kharkov River
(19,21) #Vasaloys
(34,22) #Badka River
(06,23) #Kharkov River
(24,23) #Peschanoye
(31,23) #Molodovoye
(33,23) #Badka River
(05,24) #Kharkov River
(03,25) #Kharkov
(04,25) #Kharkov
(27,25) #Bolbadka
(35,25) #Badka River
(38,25) #Khotomlya
(02,26) #Kharkov
(03,26) #Kharkov
(04,26) #Kharkov
(16,26) #Privol'noye
(31,26) #Badka River
(01,27) #Kharkov
(03,27) #Kharkov
(04,27) #Kharkov
(09,27) #Svechenko
(05,28) #Kharkov River
(06,28) #Kharkov
(23,28) #Zarozhnoye
(24,28) #Zarozhnoye
(30,28) #Badka River
(05,30) #Kharkov River
(30,30) #Badka River
