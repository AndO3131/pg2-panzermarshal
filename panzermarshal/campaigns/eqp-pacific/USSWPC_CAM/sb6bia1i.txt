BIAK 1 - May 27, 1944

Strategic Situation:
Japanese forces around Wewak have been effectively isolated, although there is still a lot of fighting in the Aitape area (between Hollandia amd Wewak) - but the campaign will continue. The next operations are intended to isolate remaining Japanese units in the western part of New Guinea and capture bases needed for the advance on the Philippines. In late May 1944 the two planned operations are Operation Straightline (the area around Sarmi, on the coast west of Hollandia), and Operation Horlicks (Biak Island, even further to the west).

Tactical Briefing:
You command the land portion of Operation Horlicks. The initial landings are in the area around Bosnik and are unopposed. Your orders are to move west and capture the three main airfields. The terrain of Biak is rough, with a long coastal escarpment that will limit movement, but control of the airfields is vital so you will have to move fast. Elements of the experienced Japanese 36th Division are present, although their strength and disposition are unknown at this time.

YOU HAVE RECEIVED ORDERS:
You should land on the cleared beaches around the villages of Bosnik and Soriari (in the east).