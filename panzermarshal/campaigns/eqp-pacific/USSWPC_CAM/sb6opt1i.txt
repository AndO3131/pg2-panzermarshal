PLAYER CHOICE SCENARIO

"General XXXX you have the option of commanding the landings on the island of Biak, or the landings in the area east of Sarmi..."

Each option is 2 scenarios.

Move the unit on the map to the hex labeled for the scenarios you want to play - go directly to the hex otherwise you may end up commanding an unexpected battle, or you may lose the campaign.