DAVAO - May 1, 1945

Strategic Situation:
Manila, and in fact Luzon, are more or less under control (although there is still a lot of fighting). Meanwhile, the Mindanao operation (and other southern Philippines operations) continues.

Tactical Briefing:
Recently a number of landings have been made in the south and west of Mindanao and most of the defenders have been pushed inland. A large Japanese detachment remains in control of the port of Davao on the eastern side of the island. Your orders are to capture Davao and then attack west to help surround the Japanese defenders in the center of the island. Meanwhile, you should link with Filipino guerilla forces that are moving in from the north.