BOFU - November 8, 1942

Strategic Situation:
After escaping The Philippines with MacArthur's staff you are assigned to command some "green" units. In July 1942 the Japanese land near Buna mission and soon they attack over the Owen Stanley mountains, using the Kokoda Trail, with the intention of capturing Port Moresby. However in mid-September, in the face of strong Australian opposition, they start withdrawing back towards Buna. 

Tactical Briefing:
Before the Japanese forces receive reinforcements we intend to counterattack. The plan is for the Australians to advance across the Kokoda trail while the US Army forces advance by land and air from the southeast.

Your inexperienced men, who were expecting a soft posting in Europe, are now in some of the worst terrain on Earth preparing for an assault on the area around Buna - which is known to be heavily defended. Before the attack begins your orders are to capture some of the outlying defenses. Your force is currently operating with almost no artillery or other support and are badly affected by sickness after the arduous journey.
