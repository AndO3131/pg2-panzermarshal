TACTICAL VICTORY

Despite vicious fighting on Wakde Island the airfield has been captured intact and is already in use. However, the area is not clear of enemy forces.