BRILLIANT VICTORY

Despite some scares, your masterful defense has stopped the assault along the Bagac-Orion Line. The battle is now a stalemate, with both the Japanese and US forces dug in along the line, but the US & Filipino forces on Bataan are low on supplies and ammunition and it is only a matter of time before the Japanese build up more forces to renew their attacks. 

However, before you have to deal with the situation, on March 9th MacArthur receives orders from the President to leave the Philippines. On March 11th he leaves for Mindanao with his family and some valued staff members (including yourself) by PT boats and finally, on March 17th, you are evacuated with MacArthur by B-17 to Australia. 

Your men have been left behind to face who knows what fate... and you will have a new command.

NOTE:

MacArthur left the Philippines on March 12, 1942. The defenders on Bataan surrendered on April 9 and the final surrender, on Corregidor, was on May 6. This is the reason you had to take a victory in this battle to continue.