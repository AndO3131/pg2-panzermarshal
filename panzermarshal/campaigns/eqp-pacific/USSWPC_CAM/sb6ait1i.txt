AITAPE 1 - July 9, 1944

Strategic Situation:
Most of the Japanese on New Guinea have been bypassed or defeated, but there are still pockets of resistance. The largest group is centered on Wewak; to the east Australians are slowly advancing from Madang, to the west the US Army is slowly moving from Aitape. The Japanese air force at Wewak has been largely neutralized by Allied bombing, but there are plenty of ground troops.

Tactical Briefing:
Aitape was captured in April 1944, at the same time as the Hollandia operation. Since then the Army has been advancing slowly east and have reached the Driniumor River, where elements of the Japanese 20th, 41st and 51st Divisions (of the 18th Army) have held up the advance. Your men have been transported east to Aitape, where you have been ordered to take over the Driniumor River line, from the coast south to Afua. Even as you take your positions there are signs that an attack is imminent...

(IMPORTANT NOTE! The only way to "win" this scenario is to take a Loss. If you do not play through all 20 turns you will lose the campaign.)