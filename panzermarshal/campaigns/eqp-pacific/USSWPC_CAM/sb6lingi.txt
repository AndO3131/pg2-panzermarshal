LINGAYEN GULF - December 23, 1941

Strategic Situation:
MacArthur's plan of holding out until reinforced by the Pacific Fleet is now in grave jeopardy. 

Tactical Briefing:
On Luzon small Japanese forces have landed in the southeast and the north. The Japanese forces in the north are small, so you have been withdrawn from the North to defend against a likely Japanese landing near Lingayen Gulf. On December 22nd the Japanese 14th Army starts landing large formations on the eastern side of Lingayen Gulf and already, by the 23rd, rumors reach you that a withdrawal is being planned. 

Your orders are to command the defensive operations at Lingayen Gulf and specifically along the Agno River. Exposed Filipino troops from the 11th Division need to be pulled back to safety at the same time.

NOTE: 
The ONLY way to win this scenario is to hold out for a Tactical Victory and then capture hex 31,42 !! Any other result is a Loss. The Japanese units in this scenario are very powerful and need to be attacked or defended against with full support.