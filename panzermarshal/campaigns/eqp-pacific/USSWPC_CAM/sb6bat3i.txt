BATAAN 3 - January 30, 1944

Strategic Situation:
The invasion of the Philippines continues. In the course of the last 2 weeks US forces landed and moved inland from Lingayen Gulf, advancing towards Manila. 

Tactical Briefing:
In advance of the assault on Manila a number of secondary landings will take place to help secure Manila Bay. On January 29th a force landed at San Narciso (which is on the west cost of Luzon north of Bataan) with the objective of cutting across the base of the Bataan Peninsula and linking with units moving inland from Lingayen Gulf - this will have the effect of isolating the Japanese defenders on the Bataan peninsula. As part of this offensive you have orders to lead a small amphibious force and capture Japanese naval facilities in Subic Bay. 

You only have some of your units available to you, since many of your men are still being shipped from Morotai.

Your secondary orders are to attack over the mountainous area north of Bataan if the force from San Narciso has not reached the area, but by now you should be familiar with this sort of terrain...