VICTORY

Your men will be replaced by a garrison and will be moved back to New Guinea. Despite the fact the the Arawe operation is considered only a small diversion, the experience gained has not gone unnoticed at HQ.