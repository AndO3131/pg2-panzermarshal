CONTENTS OF THIS DOWNLOAD:

1. 00-CONTENTS-ADDENDUM-BUGS-COPYRIGHT.TXT - This file;

2. CAMPAIGN.ZIP - English language version of the campaign;

3. EQUIPMENT.ZIP - Pacific Equipment File v4.2;

4. README.ZIP - The readme files - please read the readme first which will explain what to do with all these files.
-----------------------------------------------------------------------------------------
ADDENDUM & BUGS:

If any links in the readme are bad, the correct link should be on my web site, otherwise or email me...
-----------------------------------------------------------------------------------------
COPYRIGHT:

� Copyright 2003-2008, Steve Brown.

This campaign is the product of hundreDs of hours of work by me and my playtesters, so I hope you repsect this copyright. 

Please do not change or remove anything from this or any of the enclosed archives without permission if you make this campaign available on a web site, by ftp, on a CD or any other method.

You absolutely may NOT include this campaign on any commercially available product, or charge for a download, without my permission.

US Army New Guinea Campaign, US Southwest pacific Campaign and Steve Brown Workers' Collective are all world wide trademarks of Steve Brown. If you use this name or any other name that misleads and causes actual confusion to people as to the source of origin of your campaign or scenario, with this campaign and its designer as to source of your product, you are a potential trademark infringer!

No warranty, either express or implied is made that the campaign will work as intended, on your computer. It is being provided solely on an "as is" basis. The user by installing the campaign for use, assumes the risk of his computer system crashing. The campaign is thoroughly tested but I assume no liability whatsoever for any damage that may be caused to your computer system by use of this product. You may not modify the product in any manner, except for your own personal use.

Having said all that ... I hope you enjoy this campaigN!

Steve Brown
05/2008

steve@pg-2.com
http://www.pg-2.com