US ARMY SOUTHWEST PACIFIC CAMPAIGN (v1.0)
(Formerly known as US Army New Guinea Campaign, 1942-44)

A product of the Steve Brown Workers' Collective


1. CREDITS and CONTACT INFORMATION:

1a. Credits:
Bill Dickens (aka bismarck13) and Mike Thorne (MadMike1) created for Pacific Equipment File (although I have made many changes to the current version); Many other people, too many to mention, have created all sorts PG2 mods; Jesper Damgaard, Dennis Felling, Jan Hedstr�m, "Highlander", Mick Marchand, Chris Nies and John White were my playtesters at various times. Special thanks to the CC42 Momsters.

1b. Contact information:
Any problems, suggestions, comments or abuse should be directed to me. Any feedback on this campaign, good or bad, will be greatly appreciated - Steve Brown
steve@pg-2.com
http://www.pg-2.com
If any links go bad, please email me!

2. SIMPLE INSTALLATION INSTRUCTIONS:

For players familiar with PG2 - here is all you need to know:

Equipment file - Pacific Equipment File v4.2 or later.
Maps -   Aitape (273), Arawe (286), Bataan3 (254), Biak (174), BunaGona2 (87), Cagayan (348), Corregidor [Corigadr.map] (253), Davao (623), Hollandia (173), Lae (177), Lingayen2 (192), Luzon (248), Madang (165), Morotai (418), Olongapo (809), Pacific (294), Sarmi (287), Zamboanga (814)
PG2 Version - use at least the "unofficial" v2.20 patch.

3. INSTALLATION INSTRUCTIONS:

This readme assumes some basic knowledge of copying, moving and unzipping files in Windows and you should at least know which folder on your computer contains the program. User-made additions to Panzer General 2 are not as easy to install as the original software, but are not difficult if you read the installation instructions and use a certain amount of common sense - new maps, equipment files and other material ALL have installation instructions (some of which are included with the downloads and some of which are on the web sites). It is important to remember that while I am very happy to help with any problems - all the information you need is available, you only need to take the time to find and read it. The "Campaign Installation Instructions" page on my web site should help if you have problems.

3a. Download the campaign:
Download the campaign (yes, you most likely have done this already). This archive contains:
i. CAMPAIGN.ZIP - The US/UK version of the campaign,
ii. README.ZIP - Campaign documentation,
iii. EQUIPMENT.ZIP - Latest version of the Pacific Theater Equipment File

3b. Unzip:
Unzip files from CAMPAIGN.ZIP to your SCENARIO folder. If you have a version of the US Army New Guinea Campaign installed, just copy over the old files.

3c. Equipment file:
The Pacific Theater equipment file should be used. The latest version (v4.2) is in the EQUIPMENT.ZIP archive. You need the equipment files, and the various sound and graphics upgrades that are linked from http://www.wargamer.com/pg2campaigns/steve/pg2-usswpc.htm - if you have not installed equipment files or graphics updates before, please follow the "Upgrade Center" link, or contact me if you have questions. 

3d. Maps, download:
You need the maps Aitape (273), Arawe (286), Bataan3 (254), Biak (174), BunaGona2 (87), Cagayan (348), Corregidor [Corigadr.map] (253), Davao (623), Hollandia (173), Lae (177), Lingayen2 (192), Luzon (248), Madang (165), Morotai (418), Olongapo (809), Pacific (294), Sarmi (287), Zamboanga (814) .

Get them from http://www.wargamer.com/pg2campaigns/steve/clearinghouse/pg2-mods_n_stuff-maps.htm. In addition, a large .zip file containing all the maps for this campaign (16.87MB) is at http://www.wargamer.com/pg2campaigns/steve/pg2-usswp.htm. There are also a couple of useful map download sites linked from the above page.

3e. Maps, install:
If you do not know how to install user maps, follow the instructions EXACTLY from "How to use maps" at the above map page and you should have no problems - most campaign crash problems are related to incorrect installation of the user maps;

	3f. Video and Music:
Music that plays over the scenario briefings is available from http://www.wargamer.com/pg2campaigns/steve/pg2-usswpc.htm - this is optional. Installation instructions are included with the download;

3g. Patches & running French or German versions of PG2:
You MUST patched your program to v1.02 using the official SSI patch and you MUST also be using the "unofficial" v2.20 or newer patch. A link to the patch is at  http://www.wargamer.com/pg2campaigns/steve/pg2-usswpc.htm - installation instructions are included with the download;

3h. List of playable campaigns in PG2
This campaign will be listed as "US ARMY SOUTHWEST PACIFIC CAMPAIGN."

That's it ... start the campaign and enjoy!

4. REVISION HISTORY:

US Army New Guinea Campaign

Version 1.00 released 04/2003
Version 1.10 released 04/2003
Version 2.00 released 07/2003
Version 2.10 released 04/2004
Version 3.00 released 04/2004 (converted by Leon to his equipment file)

US Army Southwest Pacific Campaign

Version 1.00 released 05/2008

5. THE CAMPAIGN:

You command the US Army in historic battles from the fall of the Philippines (1941-42), through New Guinea (1942-44) and the return to the Philippines (1944-45). This is a campaign unlike most others; many scenarios consist of jungle/mountain/swamp fighting plus some amphibious operations against a tough enemy and are mostly infantry battles - there are no wide sweeping Panzer movements or blitzkrieg. This is a hard campaign, but it is fun :-)

6. CAMPAIGN DEVELOPMENT NOTES:

It was hard to find information about some of these battles, particularly Japanese info, but the orders of battle & general scenario design do reflect reality. One quirk of the equipment file is not all units can be transported by sea. This campaign was playtested at 100 prestige; inexperienced players may want to start at 150 or higher, which will gain you a small amount of extra prestige.

There is one scenario (Aitape 1) where you HAVE to get a loss to "win" - no other result is possible. You have to simply play through the full 20 turns to continue the campaign; it is intended as a pure defensive scenario. A loss will show up in your dossier, but consider it a victory :-)

There is a bug in the HQ after the Cagayan scenario, sometimes an auxiliary unit shows up in the core unit list, but it is NOT part of your core and will go away. I have no idea where this bug originates.

7. HINTS & TIPS:

Unit purchasing is very different in this campaign than almost any other, because of the terrain. If you want to play without my purchasing advice please ignore this section. In general you should build up a strong infantry force, with plenty of artillery, and concentrate less on tanks & other AFVs

Transport:
From New Guinea onwards, if you buy transport I recommend you buy the mule teams, which are more flexible in the jungle. The campaign is designed assuming many infantry units will be on foot so do not feel the need to give infantry transport, but it is not necessarily bad to do so. Once you return to the Philippines you should get some trucks.


Tanks:
You may not be able to afford a tank for a while and even then you will probably start out with a light tank; this is my intention since I want the early scenarios to be played with a realistic core. Tanks in this campaign are useful as infantry support, as they were in real life, however they can be vulnerable and use a lot of fuel in the bad terrain. You should not need more than 1 or 2 tanks; my preference is 1 at most.

Anti-Tank:
There are few Japanese AFVs in this campaign so anti-tank capability of not of much relevance, but you will have plenty of fortifications to destroy. US self-propelled AT units are very useful and in some respects are better buys than tanks - the M10, in particular, is worth looking at but it is more vulnerable in close terrain than a tank.

Air-Defense:
You should have at least 1 or 2 AD units. My preference is the 37mm towed units, but you will not be able to load them on sea transports. Self-propelled units are nice, but are not as deadly and have fuel problems in bad terrain. Air defense is important in this campaign but, unlike some of my campaigns, you don't need massive air defenses.

Artillery:
My personal preference is for the 75mm and 105mm Airborne units, which have 1 movement point and are very good units. There are also some affordable SP units and some reasonably priced heavier units. Whatever you decide, 2 or 3 units are necessary at a minimum.

Recon:
You have many good choices from the cheap (and vulnerable) Scouts and Jeeps to the M8 and M20 armored cars, which are useful but can be vulnerable and have fuel issues.

Infantry:
Infantry is everything in this campaign. Personally I prefer 7 or 8 regular infantry (mainly "foot" or "mechanized"). It is not really important to give transport to all units during the New Guinea scenarios. You can buy some of the better units if you prefer, but it is not absolutely necessary (however, it not a bad thing either). Paratroops will have limited use in this campaign, but they can sometimes be used.

Bombers and Fighters:
You do not need core aircraft since there are usually plenty of aux units and in any case, apart from prototypes, you will not be able to afford aircraft until the later part of the campaign (enough prestige for a bomber shouldn't be available until at least May 1944). I personally like to get a B-25 when I can afford it, but by that stage in the campaign you may prefer to spend your prestige on other equipment. Decisions... decisions..

8. FINAL NOTE:

This campaign is the result of many, years work by Steve Brown Workers' Collective and "we" consider it copyrighted. Please do not change anything on a version that you are making available to other players. You absolutely do not have permission to include this campaign with any commercial product without my permission!

9. APPENDIX 1; LIST OF SCENARIOS AND CAMPAIGN FLOW:

9a. File names for the campaign:
All scenario file names (see section 9b, below) are of the form sb6xxxx.scn (for the scenario file) and sb6xxxx.txt (for the scenario text file). The scenario intro texts are sb6xxxxi.txt. The brilliant victory, victory, tactical victory and loss texts are sb6xxxxb.txt, sb6xxxxv.txt, sb6xxxxt.txt and sb6xxxxl.txt respectively. The campaign file is camp6sb6.cam. The campaign intro file is sb6.txt.

9b. Campaign Flow: (the numbers below do not correlate to the scenario numbers in the campaign file)

List of scenarios				Campaign flow
Scenario					
Name (scenario file name)			BV	V	TV	L	
1. Cagayan Valley (sb6caga)			02	02	02	02
2. Lingayen Gulf (sb6ling)			Lose	Lose	03	Lose
3. Withdrawal to Manila (sb6with)		04	04	04	Lose
4. Bataan 1 (sb6bat1)				05	05	05	05
5. Bataan 2 (sb6bat2)				06	06	06	Lose
6. Bofu (sb6bofu)				07	07	07	Lose
7. Buna (sb6buna)				08	08	08	08
8. Salamaua 1 (sb6sa1)				09	09	09	09 
9. Salamaua 2 (sb6sal2)				10	10	10	10
10. Arawe 1 (sb6ara1)				11	11	11	11
11. Arawe 2 (sb6ara2)				12	12	12	14
12. Saidor (sb6said)				13	13	13	Lose
13. Hollandia (sb6holl)				15	15	15	Lose
14. Arawe 3 (sb6ara3)				06	06	06	Lose
15. Choice, either 16 or 18
16. Wakde Island (sb6wakd)			17	17	17	Lose
17. Sarmi (sb6sarm)				20	20	20	Lose
18. Biak 1 (sb6bia1)				19	19	19	Lose
19. Biak 2 (sb6bia2)				20	20	20	Lose
20. Aitape 1 (sb6ait1)				--	--	--	21
21. Aitape 2 (sb6ait2)				22	22	22	22
22. Aitape 3 (sb6ait3)				23	23	23	Lose
23. Morotai 1 (sb6mor1)				24	24	24	Lose
24. Morotai 2 (sb6mor2)				25	25	25	25
25. Bataan 3 (sb6bat3)				26	26	26	27
26. Corregidor (sb6corr)			27	27	27	Lose
27. Zamboanga (sb6zamb)				28	28	28	Lose
28. Davao (sb6dava)				Win!	Win!	Win!	Lose







Continued on next page (in PDF version only) . . .

