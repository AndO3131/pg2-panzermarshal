BUNA - November 16, 1942

Strategic Situation:
The Japanese have been pushed back to the coast around Buna and Gona. The Australian 7th Division will attack from the mountains towards the coast at Gona, while you are ordered to continue the move up the coast towards Buna. 

Tactical Briefing:
You still are extremely short on heavy equipment and supplies, which will be moved forward by sea and air; for now you have to fight with what you have available. The defenses around Buna are known to be strong and the defenders are fanatic, but you must succeed.