VICTORY

Morotai's airfields are already being used for operations against Japanese bases in the Philippines. You already have orders that will take most of your men to the Philippines, although Morotai must still be defended.