09. CAPE GLOUCESTER 1, 12/26/1943

Now the Solomons campaign is all but ended you will be transferred to Macarthur's Southwest Pacific command. An operation is being planned for December to occupy Cape Gloucester, on the western tip of the island of New Britain. This will have the double advantage of tightening the noose around Rabaul (which is at the eastern end of New Britain) and gaining complete control of the Vitiaz Strait, between Cape Gloucester and New Guinea (which is a vital shipping route).

This operation will have a number of phases. On December 15th Army units will be landed on the Arawe peninsula as a diversion (see my US Army New Guinea Campaign, Arawe is just SE of this battle) and on December 26th your force will be landed on the shore of Borgen Bay - your initial objective is to capture Tuluvu Airfield and to stabilize the beachhead. A small force is also ordered to be landed on the west coast to stop Japanese withdrawal from the northeast. You should expect strong Japanese counterattacks on the ground and strong air attacks, since you will be within easy striking distance of Rabaul, however you will have adequate USAAF support.

NOTE: 
You cannot land towed AA, artillery or anti-tank units on "Swamp" hexes, they must land at "Natamo" or some other non-swamp hex

NOTE: 
A loss in this scenario requires you to play it again.
