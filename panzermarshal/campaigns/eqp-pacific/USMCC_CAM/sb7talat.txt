TACTICAL VICTORY

Pull your men back to Talasea and await your replacements. The Army will take over the mopping up of New Britain; your men will prepare for the next mission.