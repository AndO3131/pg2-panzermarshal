


 
US MARINE CORPS CAMPAIGN, v2.00

A product of Steve Brown Workers' Collective

CONTENTS:
1. Credits and contact information
	1a. Credits
	1b. Contact information
2. Simple Installation Instructions
3. Installation instructions
	3a. Download the campaign
	3b. Unzip
	3c. Equipment file
	3d. Maps, download
	3e. Maps, install
	3f.  Video and Music
	3g. Patches & running German and French versions of PG2
	3h. List of playable campaigns

4. Revision history
5. The campaign
6. Campaign development notes
7. Hints and tips
8. Final note
9. Appendix 1; List of scenarios and campaign flow
	9a. File names for the campaign
	9b. Campaign flow
10. Appendix 2; Graphical depiction of campaign flow *
11. Appendix 3; Map of the Campaign *


* NOTE: only available in the PDF version of the readme

1. CREDITS and CONTACT INFORMATION:

1a. Credits:
Bill Dickens (bismarck13) and Mike Thorne (MadMike1) created the Pacific Equipment File and Bill now updates it; Many other people, too many to mention, have created all sorts PG2 mods; Peter Stone (Badpanzer), Chris Nies (PFC Lobo), Mick Marchand, Jan Hedstr�m (Pzmaniac), Dennis Felling (Armygroupdf), Jesper Damgaard (Panzergrenadier) and John White (Daerandir) were playtesters for various versions, and their input was critical. Last (but not least) Chris made the Iwo Jima scenario.

1b. Contact information:
Any problems, suggestions, comments or abuse should be directed to me. Any feedback on this campaign, good or bad, will be greatly appreciated - Steve Brown
steve@pg-2.com
http://www.pg-2.com - you should also be able to link to my site from here!

This was written in 03/2004 and updated in 07/2005. If any links go bad, please email me!

2. SIMPLE INSTALLATION INSTRUCTIONS:

For players familiar with PG2 - here is all you need to know:

Equipment file - Pacific Equipment File v4.0+ (also available on my web site).
Maps -   Cape Gloucester (313), Guadalcanal2 (175), Iwo (198), New Georgia (186), Talasea (314), Tinian (555), Torokina (297) 
PG2 Version to use - 2.10 (unofficial).

3. INSTALLATION INSTRUCTIONS:

This readme assumes some basic knowledge of copying, moving and unzipping files in Windows and you should at least know which folder on your computer contains the program. User-made additions to Panzer General 2 are not as easy to install as the original software, but are not difficult if you read the installation instructions and use a certain amount of common sense - new maps, equipment files and other material ALL have installation instructions (some of which are included with the downloads and some of which are on the web sites). It is important to remember that while I am very happy to help with any problems - all the information you need is available, you only need to take the time to find and read it. The "Campaign Installation Instructions" page on my web site should help if you have problems.

3a. Download the campaign:
Download the campaign (yes, you most likely have done this already). This archive contains:
i. CAMPAIGN.ZIP - The US/UK version of the campaign,
ii. README.ZIP - Campaign documentation,
iii. EQUIPMENT.ZIP - Latest version of the Pacific Theater Equipment File

3b. Unzip:
Unzip files from CAMPAIGN.ZIP to your SCENARIO folder. Unzip files from EQUIPMENT.ZIP to your main program folder (usually PANZER2)

3c. Equipment file:
The Pacific Equipment File must be used. The latest version is in this download and on my web site. You need the equipment files, the "soundup2005" for new sounds (which you can download from my site), icons and other graphics upgrades from www.wargamer.com/pg2campaigns/steve/pacfile/pg2-pacfile.htm and the "datup" (for the graphics and icons) - if you have not installed equipment files or graphics updates before just download everything and follow the instructions, or contact me if you have questions. The "Upgrade Center" on my web site has the basics of upgrading - http://www.wargamer.com/pg2campaigns/steve/upgrade/pg2-uc.htm.

3d. Maps, download:
You need the maps Cape Gloucester (313), Guadalcanal2 (175), Iwo (198), New Georgia (186), Talasea (314), Tinian (555), Torokina (297) - there is one other Guadalcanal map and two other Iwo Jimas so make sure you install the correct ones.

All these maps are available from the maps section of the "Clearinghouse" on my site. In addition, a large .zip file containing all the maps for this campaign (6.9MB) is at www.wargamer.com/pg2campaigns/steve/pg2-usmcc.htm. Two excellent alternate map sources are www.panzergeneral2.com and www.stahlhelm.prv.pl.

3e. Maps, install:
If you do not know how to install user maps, follow the instructions EXACTLY from "How to use maps" at the Upgrade Center and you should have no problems - most campaign crash problems are related to incorrect installation of the user maps;

3f. Video and Music:
Music that plays over the scenario briefings and an intro sequence are available from www.wargamer.com/pg2campaigns/steve/pg2-usmcc.htm - this is optional. Installation instructions are included with the downloads;

3g. Patches & running French or German versions of PG2:
You MUST first patch your program to v1.02 using the official SSI patch and you MUST also be using the "unofficial" v2.10 or higher patch (the latest version at time of writing is 2.10). Look at the "Versions & Patches" section of Builders Paradise for more information and the "Clearinghouse" on my site for the unoficial patch. The unofficial patches will only work for US & UK versions of PG2; but will also work with German and French versions if you use the "DEU/FRA to Text Converter" from the "Downloads" section of my web site.

3h. List of playable campaigns in PG2
This campaign will be listed as "US MARINE CORPS CAMPAIGN". Once you have upgraded to the �unofficial� patch, the up and down arrows of the scroll bar now work (but not the scroll bar itself), so scroll down until you see the campaign.

That�s it ... start the campaign and enjoy!

4. REVISION HISTORY:

Version 1.00 	released 03/2004
Version 1.00a 	released 04/2004
Version 1.00b 	released 12/2004 (conversion by Leonid Usachev to his Equipment File)
Version 2.00 	released 08/2005

5. THE CAMPAIGN:

This campaign follows battles of the US Marine Corps from 1942-45. The campaign starts at Guadalcanal (5 scenarios) then follows the Solomons campaign (New Georgia and Bougainville), afterwards you fight on New Britain (Cape Gloucester and Talasea) and finally move to the Central Pacific (Tinian) and finish on Iwo Jima. 

6. CAMPAIGN DEVELOPMENT NOTES and BUGS:

In many scenarios the deployment hexes for aircraft are in the Ocean - however in the strategic view the deployment hexes often are not clear. If you do not see aircraft deployment hexes zoom in and you will see them (the hex will always be marked with a flag in the ocean).

One bug is that in the strategic view US Marine Corps units that have not moved will show the New Zealand flag (to confuse things, there are some New Zealand units in a few scenarios). This is a PG2 bug and nothing can be done about it, the US flag will show on units that have moved. NOTE: This bug does not appear in v2.0.

This campaign was playtested at 100 prestige; inexperienced players may want to start at 150 or higher, which will gain you some extra prestige.

There is one scenario (Guadalcanal 2) where you HAVE to get a loss to "win" - no other result is possible. You have to simply play through the full 22 turns to continue the campaign; it is intended as a pure defensive scenario. A loss will show up in your dossier, but consider it a victory :-)

There is a bug in the HQ after the first scenario, sometimes an auxiliary unit shows up in the core unit list, but it is NOT part of your core and will go away. I have no idea where this bug originates, but this unit is not in your core.

For Version 2.0; the Pacific Theater Equipment file takes advantage of some of the capabilities of the latest Unofficial PG2 patch - please read the readme in the equipment download for more information. In particular many units now provide Support Fire (not just artillery) , which changes the nature of many scenarios and makes them much more fun. 

HINTS & TIPS:

Unit purchasing is very different in this campaign than almost any other, because of the terrain. If you want to play without my purchasing advice please ignore this section. In general you should build up a strong infantry force, with plenty of artillery, and concentrate less on tanks & other AFVs

Transport:
The campaign is designed assuming many infantry units will be on foot so do not feel the need to give infantry transport, but it is not necessarily bad to do so. In fact giving 1 or 2 units transport may sometimes help...

Tanks:
Tanks in this campaign are useful as infantry support, as they were in real life, however they can be vulnerable and use a lot of fuel in the bad terrain. You should not need more than 1 or 2 tanks; my preference is 1 at most.

Anti-Tank:
There are few Japanese AFVs in this campaign so anti-tank capability of not of much relevance, but you will have plenty of fortifications to destroy. Self-propelled AT units can be useful, and late in the war you will be able to buy flamethrower tanks (which are in the AT class) that have engineering ability. MG units are also fun and are in the AT class...

Air-Defense:
You should have at least 1 or 2 AD units. My preference is the 37mm towed units. A reasonable amount of air defense is important in this campaign but you don�t need massive air defenses. For v2.00; in the latest equipment file many good cheap AD units are available that can also be used for ground attack - see the equipment readme.

Artillery:
My personal preference is for the 4.2" motrars and 75mm & 105mm Airborne units, which have 1 movement point and are very good units. However there are heavier units and some SP units late in the war. Whatever you decide, 2 or 3 units are necessary.

Recon:
Your recon choices are limited to Scout squads, Jeeps or Scout Cars - all have their own advantages and disadvantages, but you will need at least 1. In the latest equipment file you also have command units, which provide Combat Support.

Infantry:
Infantry is everything in this campaign. You will probably have 4 or 5 USMC regular units, 1 or more of which you may want to consider upgrading to an elite unit or to a "1944" unit.

Bombers and Fighters:
You may want to consider buying a bomber or fighter squadron, and you should be able to afford at least 1 if you wish - but maybe you can use the prestige for something else?

8. FINAL NOTE:

This campaign is the result of many, many months� work by Steve Brown Workers� Collective and "we" consider it copyrighted. Please do not change anything on a version that you are making available to other players. You absolutely do not have permission to include this campaign with any commercial product without my permission!

9. APPENDIX 1; LIST OF SCENARIOS AND CAMPAIGN FLOW:

9a. File names for the campaign:
All scenario file names (see section 9b, below) are of the form sb7xxxx.scn (for the scenario file) and sb7xxxx.txt (for the scenario text file). The scenario intro texts are sb7xxxxi.txt. The brilliant victory, victory, tactical victory and loss texts are sb7xxxxb.txt, sb7xxxxv.txt, sb7xxxxt.txt and sb7xxxxl.txt respectively. There is a custom map file for the Iwo Jima scenario - sb7iwoj.map. The campaign file is camp6sb7.cam. The campaign intro file is sb7.txt.

9b. Campaign Flow:

List of scenarios						Campaign flow
Scenario	Scenario					
Number		Name (scenario file name)			BV	V	TV	L	

00		Guadalcanal 1 (sb7gua1)				01	01	01	Lose
01 		Guadalcanal 2 (sb7gua2)				--	--	--	02 **
02		Guadalcanal 3 (sb7gua3)				03	03	03	02*
03		Guadalcanal 4 (sb7gua4)				04	04	04	Lose
04		Guadalcanal 5 (sb7gua5)				05	05	05	04 *
05		New Georgia 1 (sb7geo1)				06	06	06	05 *
06		New Georgia 2 (sb7geo2)				07	07	07	06 *
07		Bougainville 1 (sb7bou1)			08	08	08	Lose
08		Bougainville 2 (sb7bou2)			09	09	09	08 *
09		Cape Gloucester 1 (sb7glo1)			10	10	10	09 *
10		Cape Gloucester 2 (sb7glo2)			11	11	11	10 *
11		Talasea (sb7tala)				12	12	12	11 *
12		Tinian (sb7tini)				13	13	13	Lose
13		Iwo Jima (sb7iwoj)				Win!	Win!	Win!	Lose ***

* A loss in this scenario requires you to play it again.							
** A loss is the only possible result									
*** This scenario was made by Chris Nies, so blame him



Continued on next page (in PDF version only) . . .