04. GUADALCANAL 5, 10/31/1942

The Japanese have been decisively defeated and should not be able to counterattack without reinforcements, so now is the time to mount an offensive. You will attack west out of the perimeter, cross the Matanikau, with your objective being Kokumbona. Meanwhile, to the east you will advance towards Koli Point, where Japanese reinforcements are expected. Soon your men will be replaced by fresh troops, but not before you finish here.

NOTE: 
A loss in this scenario requires you to play it again.