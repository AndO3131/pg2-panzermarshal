10. CAPE GLOUCESTER 2, 01/20/1944

You have captured your initial objectives and now control of the airfield and surrounding terrain. Your orders are to clear remaining opposition from the inland parts of Cape Gloucester (or at least drive them to the east), while sending patrols to the southwest and along the eastern coast. The enemy is withdrawing remaining forces to the east towards Talasea and ultimately Rabaul, but before we deal with them you must finish your mission here.

NOTE: 
You can embark units at the "Port" of Natamo if you wish to send units along the coast by sea.

NOTE: 
A loss in this scenario requires you to play it again.