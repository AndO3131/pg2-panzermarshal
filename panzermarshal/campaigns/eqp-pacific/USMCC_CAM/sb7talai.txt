11. TALASEA, 03/05/1944

The last objective on New Britain is Talasea; located on the Willaumez Peninsula over 100 miles east from Cape Gloucester. The extent of the defenses around Talasea are unknown, but are not expected to be heavy; a bold move by sea should enable us to intercept Japanese troops withdrawing from Cape Gloucester. Once captured, Talasea will be developed as a PT boat base to cover Japanese shipping routes to Rabaul and their nearby base at Cape Hoskins.
 
This will be our most easterly move, after which we will be replaced by Army 40th Division troops. Your orders are to land near Volupai, capture Talasea and send patrols along the coast in the direction of Cape Hoskins.

ORDERS: 
You must embark your units by Ocean from Linga Linga Plantation, since there is no land route to the peninsula. Your orders are to land on the beaches around Volupai on the west side of the peninsula. You should not try to land south of Volupai because of uncleared coastal minefields. Port facilities at Talasea and San Remo Plantation can be used to speed your advance.

NOTE: 
The USS Nies may sustain some damage on its way to the battle.

NOTE: 
A loss in this scenario requires you to play it again.
