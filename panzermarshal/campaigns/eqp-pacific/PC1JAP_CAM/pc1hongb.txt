China Coast: Hong Kong
Dec 1941

As you know our invasion timetable for the liberation of South East Asia is very tight.  

Your mission is to break though the mainland defence line and push onto the ports.  Once you have secured the ports you must immediately board your troops onto troopship and invade the Hong Kong island.   A small contingent of Naval ships will be available to assist in the final push onto the island.
