India: Calcutta
Sep 1942

The Marines have secured a foot hold south east of Calcutta, to date they have been unable to exploit it due to heavily fortified enemy troops.  Our High Command has come up with a brilliant plan of by passing the British defences on the India/Burma border by using your troops to reinforce the Marines position.  With the aid of your troops we should be able to break the stalemate.

Our Navy has promised to supply a strong fleet contingent to assist you.  Beware that a raiding force of British ships has been spotted in the central Indian ocean, its intentions are unknown.    

We have been reassured by a number of captured Indian troops that the whole sub continent is ripe for revolt.  Don't be surprised if the Indian subject troops mutiny from the British and welcome your force with open arms.  



