Battle of Rangoon
March 1942

The British and their subject nation troops are in complete disarray following our victories all over Asia.  Intelligent sources have confirmed that they have belatedly stated to reinforce Rangoon.  At this stage most of their formations are below strength and lacking in support arms.  Accordingly strike hard and give them no let up. 
