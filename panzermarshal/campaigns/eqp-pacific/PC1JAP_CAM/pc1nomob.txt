Russian, Northern China border: Nomonhan
July 1939

It is time to teach our old foe the Russians another lesson.  The new communist regime appears to have forgotten the previous beating we gave them in 1905.  

Your mission is to advance in strength and seize all the objectives marked on the map.  Expect strong counter attacks by mobile enemy forces.     
