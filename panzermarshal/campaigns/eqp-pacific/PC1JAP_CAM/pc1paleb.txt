Dutch East Indies: Palembang Oil Wells
Feb 1942

Your forces are to land at Palembang in the Dutch East Indies and advance inland immediately to secure the oil wells.  Dont fail, Japan needs these wells if it is to prosecute this war to its successful concussion.   

Enemy forces are a collection of Dutch, Australian and American troops.
