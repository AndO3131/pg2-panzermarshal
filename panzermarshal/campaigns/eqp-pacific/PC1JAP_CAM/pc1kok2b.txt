New Guinea: Kokoda Trail, Southern Highlands  
Mid 1942

Our forward scouts have reported that they have sighted lights coming from Port Moresby at night.  The end of the New Guinea campaign is close.  

Due to the difficult terrain we are unable to send supplies in any qualities to assist in your southern drive.  
   

