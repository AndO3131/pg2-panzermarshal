Baghdad: Iraq 
Feb 1943

As our successes continue to accumulate minor powers flock to our cause.  The Iraqi army has taken the opportunity to upraise against the Allied occupation forces in Baghdad.  Unfortunately they should have waited a few more days before revealing their intentions, now your methodical advance will need to become a rescue mission.  

The Allies are attempting to crush the Iraqi army before your arrival.  Normally we would not concern ourselves with the fate of such a weak country but the diplomatic situation is becoming complicated, the Germans are claiming that Iraq falls within their sphere of interest.  Having the Iraqi people beholden to our side would be a great advantage in our future dealings with the Germans.  

The Turks have offered to break their neutrality and assist us.  We suspect they are more interested in gaining a claim to north Iraq.  However as they are pro German there is little we can do at this stage to stop them without causing a breakdown in our dealings with the Germans.       
           


