Battle of Espritu Santo
Jan 1943

For this battle you will need to allocate forces to capture each of the enemy held islands.  Also an air field will need to be captured before you can deploy your air force.

Enemy naval forces have been sighted, they include invasion troops so we strongly recommend that you dont move the garrison from your northern island. 
