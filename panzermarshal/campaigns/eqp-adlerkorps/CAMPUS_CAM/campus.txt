United States World Campaign (1942-1945)

You take command from the invasion of North Africa (November 1942). The campaign will take you from Africa to Italy to Europe or even the USSR.  If you're really unlucky, you might find yourself fighting for your life on U.S. soil! This campaign is not considered historical and doesn't follow the operations of a certain US army division. Instead some of the most important battles fought by US Army are portrayed chronologically while other scenarios that consist of US and Soviet forces working together or against each other are purely fictional.


Author: Jan Hedstrom

Panzer Marshal Port: Nicu Pavel