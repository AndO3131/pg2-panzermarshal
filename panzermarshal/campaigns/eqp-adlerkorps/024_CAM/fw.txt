Fall Weiss [14 Armee] (1939)
FALL WEISS [14 Armee]

Version: 1.0
Player's Country: Germany
Where: Poland
When: 1939
Number of Scenarios: 4

Recommended EFile: AK2000 Beta 0.701

Campaign Designer: Santiago Fuertes


-14th German Army, Army Group South-

-General List-

September, 1939, Germany finally has broken the relationships with Poland, the invasion is on the way. The Polish will try to resist with an outdated army against the fast German Panzerdivisionen. With the 14 German Army, you will have under your command the next units; the 5th Panzer Division, the SS unit ''Germania'', the 8th Infaterie Division and a Engineer unit. Your first mission as a commander of the 14 Army, will be the assault on Krakow, one of the more industrialiced areas in Poland.