At long last! We have joined Paulus at Stalingrad! Following the winter campaign of 1943 Paulus will attack from the west and 21 Panzer from the South.

Herr general, you must manage to force a crossing of the Volga so we can attack Stalingrad from both the East and West. The end of the strugle is so close now!