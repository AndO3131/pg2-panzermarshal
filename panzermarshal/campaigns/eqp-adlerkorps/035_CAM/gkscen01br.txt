Following victory at Tobruk the 21 Panzer reinforced with a motorised regiment, an italian brigade, navy units and strong luftwaffe elements attempts to crush the british line at el alamein.
The british have massed all their might under general Montgomery to stop them.

Herr general this will be a test of your tenacity, expect the British to be dug in along the whole line and strong RAF elements have been mentioned by our intelligence agents.
Don't put too much faith to our Italian allies!

