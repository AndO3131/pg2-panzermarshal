Operation Trappenjagd was a total success. The Red Army lost 162,282 soldiers (dead and prisoners), almost all the heavy weapons and equipment and 417 airplanes.


   IAR 80

   The IAR 80 was a Romanian World War II low-wing, monoplane, all-metal construction fighter aircraft. When first flew in 1938 it was competitive with most contemporary designs like the German Bf 109E, the British Hawker Hurricane and the Supermarine Spitfire. However, production problems and lack of available armament delayed entry of the IAR 80 into service until 1941. Although there were plans to replace it fairly quickly it was forced to remain in front-line use until 1944, by which point it was entirely outdated.


   IAR 81

   The modification of  IAR.80 as a dive bomber was seen as a reasonable response, easier than designing an entirely new aircraft as well as having all of the obvious production benefits. The design was a rather modest change to the IAR.80A models that were then in production, adding a hinging bomb cradle under the centerline to throw a 225 kg (500 lb) bomb clear of the propeller (many dive bombers used a similar system). 

(from en.wikipedia.org ) 