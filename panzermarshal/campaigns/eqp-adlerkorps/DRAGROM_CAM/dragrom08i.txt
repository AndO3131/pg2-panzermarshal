     Crimean Partisans - 27 December 1943

      An important problem for the defenders of Crimea were the large partisan formations in the Yaila Mountains, which in November 1943 were estimated to be about 7-8,000 men strong. After the Red Army had reached the Perekop Isthmus, they intensified their actions, which until then had been pretty rare and involved small groups. They became so troublesome that the Romanian Mountain Corps was ordered to clear the main partisan group, 20 km east of Simferopol. The operation took place between 29 December and 4 January. 

(from www.worldwar2.ro)