
On 2 July, the southern section of Army Group South � the Romanian 3rd and 4th Armies, alongside the German 11th Army � invaded Soviet Moldavia, which was defended by the Southern Front. Counterattacks by the Front's 2nd Mechanized Corps and 9th Army were defeated, but on 9 July the Axis advance stalled along the defenses of the Soviet 18th Army between the Prut and Dniester Rivers.


FT light tank

     Seventy-six Renault FT-17 tanks were obtained by Romanian-French collaboration in 1919 and equipped the first Romanian tank battalion. 48 of these tanks were armed with a Puteaux 37mm gun and 28 were armed with a Hotchkiss 8mm machine-gun. The vehicle was operated by a crew of two: the driver and the commander, the latter handling the armament in a 360 degrees rotating turret. During the inter-war period, part of the Renault FT-17 tanks were refurbished at Leonida Works and at the Arsenal of the Army in Bucharest. At the outbreak of the war in the East, the outdated FT-17 tanks, designated "FT", formed the independent FT Tank Battalion. They were used for security duties in important industrial and urban centres of Romania (Bucharest, Ploiesti, Sibiu, Resita) and training. They played an important role in the rapid annihilation of German forces in these centres during the insurgency of 23 August 1944. All of them were confiscated by the Soviets in February 1945.
                                                     
(article from www.worldwar2.ro )

