Battle for Kishinev - 14 July 1941

      The general offensive on the Prut River line for the recovery of Bassarabia and Bukovina , named Operation Munchen, was planned for 2 July. The experimented 11th german army and the romanian 3rd and 4th armies faced the  9th and 18th soviet armies.The task of liberating Kishinev was assigned to romanian 3rd corps and german 54th corps.
