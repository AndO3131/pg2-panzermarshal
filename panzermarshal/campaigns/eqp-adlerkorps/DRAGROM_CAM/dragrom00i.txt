    Bridgehead over Prut river -29 June 1941

    On June 22, 1941, Germany launched Operation Barbarossa, attacking the Soviet Union on a wide front. Romania joined in the offensive, with Romanian troops crossing the River Prut.
