Operation Trappenjagd was a total success. The Red Army lost 162,282 soldiers (dead and prisoners), almost all the heavy weapons and equipment and 417 airplanes.

    S-79B 

    In May 1937, an order was issued to Savoia Marchetti for 24 bombers. The Italians offered the tri-motored S.M. 79, but ARR wanted that the bombers be modified. The new airplane was designated S-79B and was powered by two Romanian-built Gn�me-Rh�ne 14K engines.

   The airplane was much appreciated, so it was decided to buy the license to produce 36 new Savoias at IAR Brasov. However, the new bombers were also going to be fitted with the more powerful Jumo 211 Da engines. Eight of these new airplanes were ordered in Italy. They were designated JIS-79B (Jumo Italian S-79B). But they didn't arrive until August 1941.

    The only S-79B equipped unit at the beginning of Barbarossa in the ARR was the 1st Bomber Group. The first Savoias were shot down from the very first day. Because of heavy losses, in July 1941, one of the 2nd Bomber Group's squadrons, the 75th, was re-equipped with brand new JRS-79Bs (Jumo Roman S-79B). These went on to fight in the battle of Odessa together with the remnants of the 1st Bomber Group.

(article from www.worldwar2.ro)