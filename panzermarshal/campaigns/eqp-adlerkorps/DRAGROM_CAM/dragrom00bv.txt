
On 2 July, the southern section of Army Group South � the Romanian 3rd and 4th Armies, alongside the German 11th Army � invaded Soviet Moldavia, which was defended by the Southern Front. Counterattacks by the Front's 2nd Mechanized Corps and 9th Army were defeated, but on 9 July the Axis advance stalled along the defenses of the Soviet 18th Army between the Prut and Dniester Rivers.


R-35 light tank
 
     Only forty-one Renault R-35 tanks were received by 1939, the delivery being cut off after the fall of France in 1940. In late September 1939 a number of thirty-four Polish R-35 tanks of the 305th Battalion that have escaped into Romania were also interned in accordance with a Romanian-Polish agreement, resulting in a total of seventy-five R-35 tanks available to the Romanian Army in late 1939. The R-35 tanks equipped the 2nd Tank Regiment, set up on 1 November 1939.

   After the 1941 campaign the 2nd Tank Regiment was kept in reserve for training personnel for the 1st Tank Regiment and the security of Transnistria (the southern Ukrainian region between Dniester and Bug). A number of 30 was modernised in 1944 by replacing the 37mm gun with a 45mm anti-tank gun resulting the Vanatorul de care R-35 (R-35 tank destroyer).

    In spring of 1945 the 2nd Tank Regiment included 28 original and modernised R-35 tanks. During the fights of the Hron River valley on 26/27 March 1945, eight R-35s were destroyed and other two damaged. Further R-35s were lost during the regiment advance through Czechoslovakia and Austria, by the end of operations none surviving.

( article from www.worldwar2.ro )
