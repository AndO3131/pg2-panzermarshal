1st SS-Panzer Division LSSAH, 
SS-Brigadefuhrer Theodor Wisch.

The Battle of Caen. 
July 8th, 1944. 
Operation Charnwood.

On the 8th July the Allies launched Operation Charnwood, an operation to secure the city of Caen.  The Leibstandarte, along with other German units was rushed to the British sector and until the 9th was engaged in repulsing the attack, which cost the life of former Leibstandarte regimental commander and current Hitlerjugend commander Fritz Witt, killed in his HQ by naval gunfire.

(If playing with rules do not buy any new units, except to replace any core losses.  There is limited deployment in this scenario, as the bulk of the division had not yet reached the front.)

(Only a TV or a L is possible here.  You must hold out until the last turn and then take the roadblock hex with the reinforcement that shows up to get a TV.)
