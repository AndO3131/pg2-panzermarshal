Korsun Pocket
I/1-SS/LSSAH
II/1-SS/LSSAH
III/1-SS/LSSAH
I/2-SS/LSSAH
II/2-SS/LSSAH
III/2-SS/LSSAH
1-SS-Pio/-/LSSAH
I/1-SS-Pz/LSSAH
II/1-SS-Pz/LSSAH
1-SS-PzJg/-/LSSAH
1-SS-Fk/-/LSSAH
I/1-SS-Aty/LSSAH
II/1-SS-Aty/LSSAH
III/1-SS-Aty/LSSAH
IV/1-SS-Aty/LSSAH
1-SS-Auf/-/LSSAH
I+II/1/1st Pz.
I/1/1st Pz.
II/1/1st Pz.
I/113/1st Pz.
II/113/1st Pz.
37/-/1st Pz.
1/-/1st Pz.
I/37/1st Pz.
II+III/37/1st Pz.
III/37/1st Pz.
IV/37/1st Pz.
I/9-SS/Wiking
II/9-SS/Wiking
III/9-SS/Wiking
I/10-SS/Wiking
II/10-SS/Wiking
III/10-SS/Wiking
I+II/5-SS/Wiking
503rd Hy. Pz. Abt.
506th Hy. Pz. Abt.
III+IV/5-SS/Wiking
I+II/2/16th Pz.
I/64/16th Pz.
II/64/16th Pz.
16/-/16th Pz.
I+II/16/16th Pz.
II/16/16th Pz.
III/16/16th Pz.
I+II/39/17th Pz.
I/40/17th Pz.
II/40/17th Pz.
III/40/17th Pz.
27/-/17th Pz.
I/27/17th Pz.
I+II/27/17th Pz.
III/27/17th Pz.
Ros' River
Korsun'-Schevchenkovsky
Koschevatoje
Schenderowka
Ol'schana
Gniloi Tikitsch River
Lissjanka
Tal'noje
Tinovka
Chizhovka
Zvenigorodka
Verbovka
Tikitsch River
Schpola
Antonovka
Erki
Golmach
Kanizh
544/389th Inf.
545/389th Inf.
546/389th Inf.
245/88th Inf.
246/88th Inf.
248/88th Inf.
417/168th Inf.
429/168th Inf.
442/168th Inf.
305/198th Inf.
308/198th Inf.
326/198th Inf.
108th Pz. Rgt.
SS-Sturm. 'Walonia'
389
198
88
I+II/4/13th Pz.
I/66/13th Pz.
II/66/13th Pz.
I/93/13th Pz.
II/93/13th Pz.
13/-/13th Pz.
I/13/13th Pz.
II/13/13th Pz.
I+II/33/11th Pz.
I/110/11th Pz.
II/110/11th Pz.
I/4/11th Pz.
II/4/11th Pz.
61/-/11th Pz.
11/-/11th Pz.
I+II/76/11th Pz.
II/76/11th Pz.
III/76/11th Pz.
42nd Guards Div.
58th Mountain Div.
38th Infantry Div.
74th Infantry Div.
133rd Infantry Div.
163rd Infantry Div.
167th Infantry Div.
232nd Infantry Div.
Group Stemmermann HQ
206th Rifle Div.
110/18 TC
170/18th TC
181/18th TC
32/18th TC
1438/18th TC
1543/18th TC
1000/18th TC
292/18th TC
1694/18th TC
78/-/18th TC
736/-/18th TC
106/-/18th TC
25/29th TC
31/29th TC
32/29th TC
53/29th TC
1446/29th TC
1549/29th TC
108/29th TC
271/29th TC
75/-/29th TC
11/-/29th TC
8/20th TC
80/20th TC
155/20th TC
7/20th TC
1895/20th TC
1834/20th TC
1505/20th TC
735/-/20th TC
291/20th TC
406/-/20th TC
1711/20th TC
96/-/20th TC
53rd Guards Rgt.
1st Guards Rgt.
678th Howitzer Rgt.
689th AT Rgt.
377th Engineer Btn.
20/5th GTC
21/5th GTC
22/5th GTC
6/5th GTC
1416/5th GTC
1458/5th GTC
1462/5th GTC
80/-/5th GTC
2/5th MC
9/5th MC
45/5th MC
233/5th MC
156/5th MC
1228/5th MC
1820/5th MC
1827/5th MC
35/-/5th MC
175th Rifle Div.
254th Rifle Div.
180th Rifle Div.
193rd Rifle Div.
1st Ukrainian Front
