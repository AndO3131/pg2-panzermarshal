Despite heavy resistance from Dutch forces, we were able to cross the Ijssel defense line taking the important Arnhem city. 

While 227th Infantry division and some of the SS troops continue fighting at the Grebbe line, we are ordered to rush to the south to link up with the 9th Panzerdivision to assault Rotterdam.

