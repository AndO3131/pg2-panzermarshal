1st SS-Panzer Division LSSAH, 
SS-Brigadefuhrer Theodor Wisch.

Operation Zitadelle. 
5th July, 1943. 
Prochorovka.

In 1943 there were sharp divisions among the German high command on whether an offensive on the Eastern Front should be launched that year.  Heinz Guderian, recently called back to active duty was of the opinion that Germany would only be able to go on the offensive again in 1944, as the divisions were much too weak presently to go on the attack.  Von Manstein on the other hand, believed it essential to deal the Red Army a series of powerful blows and to remove the threat to the Ukraine and Crimea.  After months of deliberation, Hitler decided to chase one more big victory in Russia, 'that will shine like a beacon around the world'.  This would be known as 'Operation Zitadelle'.

The plan called for a converging strike by the 9th Army in the north and 4th Panzer Army to the south in the aim of enveloping the Kursk salient.  The Leibstandarte now with a new commander - Theodor Wisch, because of Dietrich's promotion - would operate in the south with 4th Panzer Army.  They were given the task of driving through the defences of the Voronezh Front before moving North-East to take Prokhorovka.

(If playing with rules do not buy any new units, except to replace any core losses.  You should deploy 6 'SS Infanterie', 2 Tanks, 4 Artillery, 1 Recon, 1 Air Defence/Flak, 1 'Sturmpioniere' and 2 anti-tank units.)
