ORAN (8 NOVEMBER 1942)

Finally your division will see some action, General!  The Axis armies are still in North Africa but we have plans to get them out of there.  Montgomery is driving them towards Tunisia from Egypt.  We, with British support, will land at the Vichy French North African colonies and create a huge pincer movement to trap Rommel.

Your division will land near Oran and be supported by elements of 1st Armored Division. Air and naval assets will be brought in by the British.  The landing beaches have been designated west and east of Oran. From these you will advance on Oran and capture the port.  A small British raiding party has been send directly to the naval facilities of Oran but we lost contact with them several hours ago.

It is uncertain how tenacious the resistance will be from the Vichy French.  But in any case, they do have a considerable amount of war material  at their disposal should they decide to defend themselves.  Do not underestimate them!

IF PLAYING WITH RULES: Buy 9 INFANTRY 42 with GMC 2.5t as transport, 3 105mm How M2 with GMC 2.t as transport, 1 155mm How M1 with White 6t as transport, 1 recon unit, and 1 AT unit.
