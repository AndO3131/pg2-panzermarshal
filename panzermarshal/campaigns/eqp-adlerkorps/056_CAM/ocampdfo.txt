Big Red One US 1st Infantry Division (1943-1945)


The 1st Infantry Division entered combat in World War II as part of Operation Torch, the invasion of North Africa.  It was the first American campaign against the Axis powers.  The Division was essentially in the battles at El Guettar and the final actions at Bizerte.  The Division then moved on to take Sicily in "Operation Husky." It stormed ashore at Gela, July 10, 1943, and quickly overpowered the Italian defenses and a powerful German armored counterattack.  The Fighting First advanced on to capture Troina and opened the Allied road to the straits of Messina. 


When that campaign was over, the division returned to England to prepare for the Normandy invasion.  It was the division that stormed Omaha Beach on D-Day, some units suffering 30 percent casualties in the first hour.  It secured Formigny and Caumont at the beachhead. The division followed up the St. Lo breakthrough with an attack on Marigny, 27 July 1944, and then drove across France in a continuous offensive, reaching the German border at Aachen in September.  The division laid siege to Aachen, taking the city after a direct assault, 21 October 1944. 


The First then attacked east of Aachen through Hurtgen Forest, driving to the Roer, and then moved to a rest area 7 December for its first real rest in 6 months' combat.  When the Wacht Am Rhein offensive (commonly called the Battle of the Bulge) suddenly broke loose, 16 December the division raced to the Ardennes, and fought continuously from 17 December 1944 to 28 January 1945.  They were instrumental in helping to blunt and turn back the German offensive. 


Thereupon, the division attacked and again breached the Siegfried Line, fought across the Roer, 23 February 1945, and drove on to the Rhine, crossing at the Remagen bridgehead, 15 and 16 March 1945.  The division broke out of the bridgehead and took part in the encirclement of the Ruhr Pocket.  They then captured Paderborn, pushed through the Harz Mountains, and were in Czechoslovakia, when the war in Europe ended.  Sixteen members of the division were awarded the Medal of Honor.


Campaign Designer: Dennis Felling

Playtesters: Steve Brown, 1Terminator and Technique.