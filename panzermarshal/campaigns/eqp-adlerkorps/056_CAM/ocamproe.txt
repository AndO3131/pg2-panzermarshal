Roer Crossing
Westwall
Schmidt
Roer River
Kall River
Kommerscheidt
Harscheidt
Vossenack
Brandenberg
Bergstein
Wittscheidt
Simonskall
Raffelsbrand
Lammersdorf
Germeter
Paustenbach
Village
Huertgen Forest: FOREST
I/5/3rd FJ Div.
II/5/3rd FJ Div.
I/9/3rd FJ Div.
II/9/3rd FJ Div.
I/8/3rd FJ Div.
II/8/3rd FJ Div.
I/3/3rd FJ Div.
II/3/3rd FJ Div.
Mtr/9/3rd FJ Div.
Mtr/5/3rd FJ Div.
MG/9/3rd FJ Div.
1/3/-/3rd FJ Div.
2/3/-/3rd FJ Div.
I/1053/85th Inf. Div.
II/1053/85th Inf. Div.
III/1053/85th Inf. Div.
III/1054/85th Inf. Div.
I/1054/85th Inf. Div.
II/1054/85th Inf. Div.
I/1064/85th Inf. Div.
II/1064/85th Inf. Div.
III/1064/85th Inf. Div.
I/185/85th Inf. Div.
II/185/85th Inf. Div.
III/185/85th Inf. Div.
185/-/85th Inf. Div.
III/5/3rd FJ Div.
III/9/3rd FJ Div.
III/8/3rd FJ Div.
FlaK/-/- [LXXIV]
Sturmartillerie-Brigade 905 
III/JG27
I/9/2nd Inf. Div.
II/9/2nd Inf. Div.
III/9/2nd Inf. Div.
I/23/2nd Inf. Div.
II/23/2nd Inf. Div.
III/23/2nd Inf. Div.
I/38/2nd Inf. Div.
II/38/2nd Inf. Div.
III/38/2nd Inf. Div.
12/-/2nd Inf. Div.
15/-/2nd Inf. Div.
37/-/2nd Inf. Div.
741/-/2nd Inf. Div.
612/-/2nd Inf. Div.
462/-/2nd Inf. Div.
USAAF 355 Squadron
USAAF 356 Squadron
USAAF 643 Squadron
Kommerscheidt Volkssturm
Bergstein Volkssturm
Brandenberg Volkssturm
Forward USAAF Base
