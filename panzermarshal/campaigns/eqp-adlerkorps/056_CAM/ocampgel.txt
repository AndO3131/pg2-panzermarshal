Gela
Gela River
Caltagirone
Butera
Acate River
Gela
Irminio River
Comiso
Ragusa
Scoglitti
18th Coastal Regiment
Niscemi
Gela River: Ponte Olivo
HILL: Piano Lupo
Village
2/505 Para/82nd AB Div.
1/505 Para/82nd AB Div.
4/505 Para/82nd AB Div.
3/505 Para/82nd AB Div.
I/34/4th Livorno Div.
III/34/4th Livorno Div.
28th Reg/4th Livorno Div.
Mobile Group "E"
Mobile Group "G"
I/76/54th Napoli Div.
II/76/54th Napoli Div.
I/382nd Inf. Reg.
II/382nd Inf. Reg.
I/Pz Arty/HG Div.
II/Pz Arty/HG Div.
III/Pz Arty/HG Div.
FlaK Reg/HG Div.
StuG Co/HG Div.
Field HQ/HG Div.
I/Pz/HG Div.
II/Pz/HG Div.
Aufklarer/HG Div.
Pio/-/HG Div.
I/1/HG Div.
II/1/HG Div.
I/2/HG Div.
III/Gruppe Fullreide
Tiger Co/HG Div.
313th FlaK Abt. [XIV Pz]
II/34/4th Livorno Div.
II/JG51
III/JG27
III/KG54
II/KG26
8./SchG2
505/82nd Airborne Div.
376/-/82nd Airborne Div.
I/180/45 Inf. Div.
II/180/45 Inf. Div.
III/180/45 Inf. Div.
I/157/45 Inf. Div.
II/157/45 Inf. Div.
III/157/45 Inf. Div.
I/179/45 Inf. Div.
II/179/45 Inf. Div.
III/179/45 Inf. Div.
160/-/45 Inf. Div.
171/-/45 Inf. Div.
45/-/45 Inf. Div.
1st Ranger Bn
4th Ranger Bn
USS Boise
USS Savannah
USS Philadelphia
USS Earle
USS Mervine
USS Shubrick
USS Jeffers
HMS Indomitable
66/CCA/2nd Arm. Div.
USAAF 58 Squadron
USAAF 59 Squadron
USAAF 91 Squadron
USAAF 93 Squadron
USAAF 86 Squadron
USAAF 97 Squadron
