ARDENNES COUNTERATTACK (17 JANUARY 1945)

The tide has turned.  The German offensive did not even reach the Meuse River and Bastogne held out under its siege.  The US Army will strike back and avenge our defeat! The US Third Army is relieving Bastogne in the south and US First Army will strike the German bulge from the north.

Your 1st Infantry Division is still dug in near Butgenbach but has received orders to advance. You must drive the enemy back to Germany.  The Germans in your sector have had time to prepare defensive positions but the good news is that most of the German panzer divisions seem to be located near Bastogne.  Your sector will most likely be defended by infantry. Panzers or not, the Big Red One will be victorious!

IF PLAYING WITH RULES: Do not upgrade any units except recon and anti-tank units. Your tank may be upgraded but it should be a 1-range tank. Your core should be 4 ARTY (1 155m M1, 3 105mm M2), 9 INF (44 Infantry), 1 RECON, 1 AT, 1 TANK (range 1). 