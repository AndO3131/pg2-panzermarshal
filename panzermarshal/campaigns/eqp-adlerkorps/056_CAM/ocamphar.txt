Harz Mountains
Harz Mountains: MOUNTAIN
Osterode am Harz
Sose River
Herzberg
Bad Lauterberg
SS Pz Bgd "Westfalen"
Freiheit
Dorste
Gieboldehausen
Silkerode
Bockelnhagen
Lonau
Bartofelde
Woffleben
Lerbach
Lasfelde
Village
Wildmann
KG 116th Pz Division
KG 9th Pz Division
989/277th Inf. Div. (-)
990/277th Inf. Div. (-)
991/277th Inf. Div. (-)
277/277th Inf. Div. (-)
KG 326th Inf. Div.
77/26th Inf. Div. (-)
78/26th Inf. Div. (-)
39/26th Inf. Div. (-)
26/26th Inf. Div. (-)
26/-/26th Inf. Div. (-)
KG Fellner
Division Ettner
Division Heidenreich
Division Grosskreuz
Osterode Garnison
I/36/3rd Arm. Div. (CCA)
II/36/3rd Arm. Div. (CCA)
32/CCA/3rd Arm. Div.
83/-/3rd Arm. Div. (CCA)
67/CCA/3rd Arm. Div.
USAAF 509 Squadron
USAAF 510 Squadron
Harz Redoubt
