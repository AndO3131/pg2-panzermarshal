ARDENNES (20 DECEMBER 1944)

Four days ago the German Army unleashed a major offensive on the Western Front. Through weakly defended sectors in the Ardennes, the Germans have broken through and are heading west.  Divisions from all over France are being rushed to the Ardennes to stop the Germans in their tracks.

The 1st Infantry Division will be deployed on the right shoulder of the German advance. Two divisions the 99th and 2nd have already been rushed south to the Elsenborn ridge.  This will prevent the Germans from using this route.  The Big Red One will take position next to the 2nd Infantry Division and prevent the enemy from attacking the US First Army from the north or from behind.

Your division is still underway and only elements have reached their final positions near the small Belgian town of Butgenbach.  You will defend this village no matter what.  You must hold the line here.  The Germans can not be allowed to advance north!  You can expect seasoned veterans from both the Western and the Eastern Front supplied with the latest firearms, tanks and other war material.  Good luck.

IF PLAYING WITH RULES: Do not upgrade any units except recon and anti-tank units. Your tank may be upgraded but it should be a 1-range tank. Your core should be 4 ARTY (1 155m M1, 3 105mm M2), 9 INF (44 Infantry), 1 RECON, 1 AT, 1 TANK (range 1). 