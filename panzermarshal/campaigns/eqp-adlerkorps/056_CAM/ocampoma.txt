D-Day: Omaha Beach
Vierville
St. Laurent
Colleville
Fox Green Beach
Easy Red Beach
Charlie Beach
Easy Green Beach
Dog Red Beach
Dog White
Dog Green Beach
C/2nd Ranger Bn
2 Ranger Bn (-)
5 Ranger Bn
A/I/116/29th Inf. Div.
G/I/116/29th Inf. Div.
F/I/116/29th Inf. Div.
E/I/116/29th Inf. Div.
743d Tank Bn
741st Tank Bn
121st Engineer Combat Bn
745th Tank Bn
Widerstandsnest 63
Widerstandsnest 62
Widerstandsnest 61
Widerstandsnest 69a
Widerstandsnest 67
Widerstandsnest 66
Widerstandsnest 68
Widerstandsnest 70
Widerstandsnest 71
Widerstandsnest 64
Widerstandsnest 65
Widerstandsnest 69b
I/726/716th Inf. Div.
II/726/716th Inf. Div.
I/914/352nd Inf. Div.
II/914/352nd Inf. Div.
I/916/352nd Inf. Div.
II/916/352nd Inf. Div.
I/915/352nd Inf. Div.
II/915/352nd Inf. Div.
352/-/352nd Inf. Div.
I/352/352nd Inf. Div.
IV/352/352nd Inf. Div.
1/352/-/352nd Inf. Div.
2/352/-/352nd Inf. Div.
I/115/29th Inf. Div.
II/115/29th Inf. Div.
III/115/29th Inf. Div.
II/116/29th Inf. Div.
III/116/29th Inf. Div.
I/116/29th Inf. Div. (-)
HMS Glasgow
USS Texas
Atlantikwall
Flak-Abteilung 835 
Flak-Abteilung 931 
29/-/29th Inf. Div.
110/-/29th Inf. Div.
111/-/29th Inf. Div.
IX Fighter Command
USAAF 640 Squadron
USAAF 404 Squadron
USAAF 393 Squadron
USAAF 392 Squadron
USAAF 391 Squadron
Airfields in Great Britain
