MONS (3 SEPTEMBER 1944)

After the victories at Avranches and the destruction of many German divisions at the Falaise pocket, the Allied armies have now liberated most of France with little or no opposition from the German army.  Still a significant number of troops, mostly veterans, have escaped the trap set at Falaise pocket and are making their way back to Germany. Most of these troops have left behind most their heavy equipment and are nowhere as mobile as the Allied armies. We must prevent them from reaching Germany and being able to refit in the Heimat.

Your division has just crossed the border of France into Belgium.  Elements of the US 3rd Armored Division are close to the Belgian town of Mons where serious resistance has been encountered.  We believe that the Germans are using Mons as an escape route for units fleeing out of France.  Your 1st Infantry Division must join up with 3rd Armored and seal the gap through which the Germans are escaping.  Let us not repeat the errors made at Falaise and again let many Germans escape!

IF PLAYING WITH RULES: Do not upgrade any units except recon and anti-tank units. Your tank may be upgraded but it should be a 1-range tank. Your core should be 4 ARTY (1 155m M1, 3 105mm M2), 9 INF (44 Infantry), 1 RECON, 1 AT, 1 TANK (range 1).
