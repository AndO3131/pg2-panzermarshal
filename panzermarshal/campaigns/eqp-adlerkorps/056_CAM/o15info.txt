HARZ MOUNTAINS (12 APRIL 1945)

The war is almost over.  We have trapped a huge German army in the Ruhr pocket and are advancing across Germany with very little opposition.  Montgomery is near Hamburg and Patton will soon be in Munich.  Sadly, Eisenhower has directed not to advance further than the Elbe River and leave Berlin to the Soviet armies.

There is one major obstacle in central Germany before reaching the Elbe River and that is the Harz Mountains.  Our plan is to go around these mountains and leave the mopping up of this area to other forces.  However, we must make the roads leading east safe.

The Big Red One will advance along the southern hills of the Harz.  The Germans have concentrated forces in the Harz to relieve the Ruhr pocket.  That is only an illusion but the forces there and will most likely furiously defend the passes.

IF PLAYING WITH RULES: Do not upgrade any units except recon and anti-tank units. Your tank may now also be upgraded to a 2-range tank. Your core should be 4 ARTY (1 155m M1, 3 105mm M2), 9 INF (44 Infantry), 1 RECON, 1 AT, 1 TANK (range 2).