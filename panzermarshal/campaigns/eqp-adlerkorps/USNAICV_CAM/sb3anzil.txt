LOSS

TO: Commanding General.
SUBJECT: Failure of OPERATION SHINGLE.
  1. Needless to say, your conduct of the landings was disastrous.
  2. The remnants of your Corps will be reassigned to other commands.
  3. We have read your suggestions for future conduct of Italian operations and will consider them. You, however, are being assigned to a command in NORTHERN ALASKA, which should be more suited to your skills.

(The campaign ends here...)