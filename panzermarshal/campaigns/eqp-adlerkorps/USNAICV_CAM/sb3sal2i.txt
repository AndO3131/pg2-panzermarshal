23. SALERNO 2, 09/12/1943

TO: Commanding General.
SUBJECT: German counterattack.
  1. German forces are attacking all along the line, from SALERNO and vicinity in the north to east of ALTAVILLA in the south. Intelligence reports suggest that elements of a number of different German formations are being rushed to the area - including some experienced Panzer divisions.
  2. The attacks seem to be concentrated in the center, directly aimed at the British 56th Division and south in your sector - although tanks have been spotted in the north. Hold your positions and delay the German offensive. Any withdrawal should be made with the understanding that the beachhead is at risk.
  3. Landing craft are at a premium, so now is the time to redeploy your men from south to north...