02. MEDJEZ EL BAB 1, 12/06/1942

TO: Commanding General.
SUBJECT: Counterattack ordered at MEDJEZ EL BAB.
  1. Elements of a number of German units have launched a counterattack aimed at capturing MEDJEZ EL BAB.
  2. MEDJEZ EL BAB must be held and the TEBOURBA Gap must be kept open ...
