LOSS

"I think it is best you hear this direct from me - you are fired as commander of this Corps. I have tried to use every bit of influence possible but this is just too much of a disaster. You should report to HQ for re-assignment..."