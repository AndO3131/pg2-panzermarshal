TACTICAL VICTORY

TO: Commanding General.
SUBJECT: The fall of MESSINA.
  1. In regards to the attached Intelligence reports, it appears most of the German and Italian defenders of MESSINA escaped over the last few nights.
  2. As soon as the situation is stabilized you should report to me to discuss your role in the invasion of the mainland...
