LOSS

TO: Commanding General.
SUBJECT: Withdrawal from FERIANA and SBEITLA positions.
  1. Our positions in southern Tunisia are not tenable. Your orders are to pull back to Algeria to a more defensible position around BONE.