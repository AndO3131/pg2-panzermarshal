LOSS

TO: Commanding General.
SUBJECT: Failure of the ORAN invasion.
  1. Your performance commanding the assault on ORAN has placed the invasion at risk, you are being removed from your command effective immediately.
  2. Your replacement has been assigned, you should place your command in the hands of your second in command until the new General arrives ...
