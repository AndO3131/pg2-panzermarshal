TACTICAL VICTORY

TO: Commanding General.
SUBJECT: Victory in TUNISIA.
  1. All resistance has ended in the north and the British and French armies are mopping up pockets of resistance near TUNIS.
  2. You are ordered to report to HQ to discuss plans for your future, your lack of agression in the BIZERTE attack is problematical and you should be prepared to defend your actions.