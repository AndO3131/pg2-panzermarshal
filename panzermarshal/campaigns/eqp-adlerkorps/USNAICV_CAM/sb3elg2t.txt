TACTICAL VICTORY

TO: Commanding General.
SUBJECT: Victory at EL GUETTAR.
  1. There is some disappoinment here that you did not reach the coast, however at least 2 Panzer Divisions have been kept busy on your front, which has relieved pressure on the British at WADI AKARIT.
  2. You should prepare for movement to the north, where you will command part of the latest offensive to capture TUNIS...