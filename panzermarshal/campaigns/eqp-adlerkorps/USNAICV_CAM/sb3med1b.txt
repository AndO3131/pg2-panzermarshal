BRILLIANT VICTORY

TO: Commanding General.
SUBJECT: Victory at MEDJEZ EL BAB.
  1. You are officially commended for your defense of MEDJEZ EL BAB. 
  2. Despite their criticism of our lack of experience you are ordered to work as amicably as possible with British commanders. 
  3. The bulk of the army has been directed to southern Tunisia, but you will remain near MEDJEZ EL BAB as a reserve for the upcoming assault on TUNIS that is planned within 2 weeks.

