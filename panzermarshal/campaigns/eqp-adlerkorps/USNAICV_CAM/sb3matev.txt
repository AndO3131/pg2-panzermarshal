VICTORY

TO: Commanding General.
SUBJECT: Capture of MATEUR.
  1. Despite Operation VULCAN not gaining all its objectives, particularly TUNIS, your excellent performance has been noted.
  2. A second offensive, Operation STRIKE, will be launched to capture TUNIS. You should support the attack and continue your assault on BIZERTE.
  3. A good performance here will ensure you have a command in the invasion of ITALY.