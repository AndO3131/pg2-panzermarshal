OPERATION TORCH TASK-FORCE CHOICE

"General xxxxx, you have been chosen to command one of the landings of Operation Torch"

You, the player, can determine which "Torch" landing you command! You make your choice by moving to the Task force you wish to join - Western or Center.

PLEASE NOTE: Go directly to your choice, otherwise you may end up commanding an unexpected battle!
