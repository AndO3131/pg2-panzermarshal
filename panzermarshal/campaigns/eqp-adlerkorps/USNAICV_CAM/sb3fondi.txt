13. FONDOUK GAP, 4/07/1943

"Welcome, gentlemen, it is an honour to have your group attached to the British army - tea?

Your mission is to help capture the pass known as the Fondouk Gap, near the village of Fondouk el Aouareb. If we gain control of this pass we will then be in a position to drive to the coast and cut off enemy forces in southern Tunisia. You will have the assistance of British and French units, but the French and our men will not be in position for a few days. In the meantime, your orders are to attack northeast from your positions at Hadjeb el Aioun. 

Your colleagues further to the south are attacking in the area of El Guettar which, coupled with this attack and the advance of our own 8th Army from Mareth, should greatly weaken the enemy. The end of the campaign is in sight."