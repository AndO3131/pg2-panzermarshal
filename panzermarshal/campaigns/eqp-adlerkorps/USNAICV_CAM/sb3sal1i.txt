22. SALERNO 1, 09/09/1943

"Commander, the following information is for your ears only. The Italian government has agreed to a secret surrender of their forces. The surrender will take place around September 3. On the basis of this, and after much deliberation, we have decided where beachheads are to be established on the Italian mainland. 

On September 3 the first invasion, Operation Baytown, will cross the Straits of Messina and capture Reggio - their task will be to push up the "toe" of Italy. A few days later we hope to launch Operation Slapstick to land at Taranto and oversee the surrender of the Italian fleet, which will sail to Malta. The major landing will be Operation Avalanche on September 9, which will consist of landings in the Bay of Salerno by 6th Corps, the British 10th Corps and other units.

You will command the initial landings of Operation Avalanche... I cannot emphasize too much the importance of establishing a secure beachhead so we can land heavier forces and advance on Naples."