VICTORY

TO: Commanding General.
SUBJECT: Victory at BONE.
  1. You are ordered to advance towards TUNIS, our victory here will be followed up with an assault on TUNIS and BIZERTE.
  2. On a personal note, you and your men should be commended for the masterful defense and countereattack at BONE. The Axis offensive has been stopped at Bone and the Germans and Italian armies are in retreat, follow up your victory and do not let the enemy rest.