BRILLIANT VICTORY

TO: Commanding General.
SUBJECT: Victory at KASSERINE.
  1. Despite serious losses we have succeeded all along the front. The British 8th army is advancing into Tunisia and our positions here and further north have held.
  2. The series of defensive actions fought in the preceeding 10 days have paid off, the lessons you and your men have learned about the enemy will be very useful in the future. 
  3. You will remain in the KASSERINE area and build up your strength in preparation for future offensives...