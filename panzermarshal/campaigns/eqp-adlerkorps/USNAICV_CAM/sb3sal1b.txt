BRILLIANT VICTORY

TO: Commanding General.
SUBJECT: Operation AVALANCHE.
  1. Despite the fact that the beachhead is smaller than planned, your latest reports are encouraging. 
  2. Intelligence reports suggest that German units are moving south so I recommend you hasten the preparation of defensive positions and bring your equipment ashore...
