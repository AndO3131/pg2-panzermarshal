07. KASSERINE PASS, 02/19/1943

TO: Commanding General.
SUBJECT: Defense of KASSERINE and SBIBA positions.
  1. German and Italian forces have captured KASSERINE and are already sending units into the pass, another unknown force is advancing up the road to SBIBA from SBEITLA while the road to THALA is also under attack. 
  2. You have command of all Allied forces on the front effective immediately. Stop the offensive. Retreat is not an option. 
  3. Reinforcements will be sent forward as we can from the north and north-west. It is now time to prove your boast that you can beat Rommel at his own game...
