06. SBEITLA & FERIANA, 02/17/1943

TO: Commanding General.
SUBJECT: Delaying action at SBEITLA and FERIANA.
  1. Your defense and counterattack around SIDI BOU ZID has paid dividends, although with limited results, but we have had time to prepare defenses in the KASSERINE area. 
  2. Elements of the Africa Corps are advancing north-west towards FERIANA from GAFSA, the 5th Panzer Army is advancing west from SIDI BOU ZID towards our positions around SBEITLA.
  3. Your orders are to hold the areas around FERIANA and SBEITLA and bring reinforcements forward as needed to ensure a safe withdrawal for as many units as possible. I am aware that your forces are spread very thin but defenses are being built up around KASSERINE PASS so the longer you can hold these positions better.