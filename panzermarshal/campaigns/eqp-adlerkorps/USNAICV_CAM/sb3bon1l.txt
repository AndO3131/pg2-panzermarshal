LOSS

"Yes, sir, a complete failure. As I speak we are evacuating our forward units further west into Algeria. We will find somewhere to make a stand, but you had better send us everything you have or the North African theater is lost."