TACTICAL VICTORY

TO: Commanding General.
SUBJECT: Victory at GELA.
  1. The worst is over, your expert defense of the German counterattack has not gone unnoticed. We now have a foothold in Europe but the offensive is far from over...