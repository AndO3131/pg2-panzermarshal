Persia - 1941

Early in the morning of June 22, 1941, the troops of the German-Fascist Wehrmancht attacked Soviet border forces along the entire line extending from the Baltic Sea to the Black Sea.  Bad for the Russians; good for us.  I shudder to think of the outcome had those divisions been sent south instead of east. However, there is now the prospect that German troops might eventually descend on the Near East via the Caucasus.

Coincidentally, it has come to our attention that it was the German embassy in Teheran that had helped finance the recent coup attemt in Iraq and it was to Teheran that the defeated Iraqi coupists fled.  It is therefore imperative that Great Britain move into Iran in order to open up communications to the Soviet Union from the Persian Gulf and "to uproot German influence and forestall enemy action" in the area.

On 25 August British troops in the south and Soviet troops in the north are to invade Iran.  Paiforce, comprised of the 8th and 10 Div (Indian), will secure the port of Abadan and advance north. Hazel Force (9 Armour Bde, 2 Armour Bde (Indian), 21 Bde (Indian)) will attack directly east toward Teheran.  I am aware of your previous history with the Reds but I remind you that they are now our allies.  Insure that there are no ... accidents. 

(Note: Only air-mobile units can be used here.)