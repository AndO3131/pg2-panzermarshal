The Levant - Beirut - 1941

After evacuating Damascus, Colonel Keime's battle weary French force of V/1st Moroccan, I & III/17th Senegalese, III/24th Colonial and I & III/29th Algerian battalions withdrew through the mountains to the Barada Gorge, dominating the Beirut-Damascus Road.  16th  Bde advance from Damascus to Beirut was consequently halted   by Jebel Mazar, towering 1600 feet above the road. 

Your forces will assist 6 Div, supported by elements of 5 Bde (Indian) and Free French units  to advance east toward Saida, north of Beirut on the coast.  7 Div is to advance north from the Damour river toward Beirut.  Habforce and 10 Div (Indian) will continue to advance on Homs and Aleppo in the north.  The capture of these vital towns will make the French positon unteniable.

