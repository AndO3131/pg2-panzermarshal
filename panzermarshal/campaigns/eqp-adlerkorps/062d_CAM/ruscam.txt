The Great Patriotic War (1942-1945)


This is a semi-historical campaign of intermediate level, portraying a Soviet Guards Tank Corps of a Shock Army. You will face many heavy battles and there is enough prestige to buy plenty of artillery and an airforce which is historical for the Soviets. With the higher prestige levels it is also able to replace lost units and play through even losing units, which is historical as well.

"The Soviets developed the concept of deep battle and by 1936 it had become part of the Red Army Field Regulations. Deep operations had two phases; the tactical deep battle, followed by the exploitation of tactical success, known as the conduct of deep battle operations. Deep battle envisaged the breaking of the enemy's forward defences, or tactical zones, for fresh uncommitted mobile operational reserves to exploit by breaking into the strategic depth of an enemy front. The goal of a deep operation was to inflict a decisive strategic defeat on the enemy and render the defence of their front more difficult or impossible. Unlike most other doctrines, deep battle stressed combined arms cooperation at all levels: strategic, operational, and tactical."

Za Rodinu! Drive the fascist beasts from the Motherland!


Campaign Designer: RC (Bob C)
Playtesters: Shikaka, Jan Hedstrom, Ze "JRM" Carneiro, Dirk Oberlin and Drag D