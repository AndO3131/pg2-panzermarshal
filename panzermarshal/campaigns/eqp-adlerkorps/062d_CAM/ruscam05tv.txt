The Soviet forces pressed back the German defenders and liberated Orel. Operation Kutuzov set the stage for the liberation of Smolensk in September 1943. The Germans were greatly weakened by this thrust, and thus were unable to stop further Soviet advances westwards.

