Koufra

25 Feb 1941

This was the first military action of Free France: 300 men and 1 (one!) 75mm cannon against superior Italian forces managed to take the Koufra oasis overnight, after an 400Km-long epic desert journey.  In this scenario appears the bulk of the core army, which is supposedly as mobile and powerful as the British desert commandos of the Long Range Desert Group.
