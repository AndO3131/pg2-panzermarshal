Operation Dragoon

16 July 1944

In coordination with Operation Overlord, French and American Forces land in Mediterranean France, considered to be more lightly defended by the Germans. Their advance is aimed at the industrial region around Lyon and far more rapid than in Normandy, threatening the lines of supply and communications for German defenders to the north. 
                                                                                                          