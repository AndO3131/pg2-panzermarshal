On to Sidi Barrani
Bardia
Sidi Azeiz
Sidi Barrani
Fort Capuzzo
Sollum
Halfaya Pass
Buq Buq
Sofafi
F/4RHA/SpGp/7Arm WDF
F/4RHA/SpGp/7Arm WDF
C/4RHA/SpGp/7Arm WDF
11H/-/7Arm WDF
1/3/Coldm Gds/- WDF
2/3/Coldm Gds/- WDF
3/3/Coldm Gds/- WDF
80Sqn WDAF
112Sqn WDAF
113Sqn WDAF
Airfield Defences
Sidi Barrani Airfield
2/RB/SpGp/7Arm WDF
3RHA/-/7Arm WDF
106RHA/-/7Arm WDF
ATCo/-/-/1Libica [LDG]
ATCo/-/-/64Catanzaro [LDG]
VI/1LAG/1Libica [LDG]
VII/1LAG/1Libica [LDG]
2/202/2 28Ottobre [XXIII]
Aty/-/231/2 28Ott. [XXIII]
6&7/-/1LAG/1Libica [LDG]
AACo/-/-/25 V
AACo/-/1LAG/1Libica [LDG]
1/1LIG/1Libica [LDG]
2/1LIG/1Libica [LDG]
3/1LIG/1Libica [LDG]
1/2LIG/1Libica [LDG]
2/2LIG/1Libica [LDG]
3/2LIG/1Libica [LDG]
Med/Misto/-/Maletti [LDG]
Lt/Misto/-/Maletti [LDG]
I/4/1RC [XXIII]
XXI/4/1RC [XXIII]
LXII/4/1RC [XXIII]
LXIII/4/1RC [XXIII]
LXII/-/62Marmarica [XXIII]
LXII/-/62Marmarica [XXIII]
62/-/-/62Marmarica [XXIII]
ATK/-/-/62Marmarica [XXIII]
ATK/-/-/62Marmarica [XXIII]
1/44/62Marmarica [XXIII]
2/44/62Marmarica [XXIII]
3/44/62Marmarica [XXIII]
1/115/62Marmarica [XXIII]
2/115/62Marmarica [XXIII]
3/115/62Marmarica [XXIII]
1/116/62Marmarica [XXIII]
2/116/62Marmarica [XXIII]
3/116/62Marmarica [XXIII]
LXII/-/62Marmarica [XXIII]
LXIII/-/63Cirene [XXIII]
63/-/-/63Cirene [XXIII]
1/45/63Cirene [XXIII]
2/45/63Cirene [XXIII]
3/45/63Cirene [XXIII]
1/157/63Cirene [XXIII]
2/157/63Cirene [XXIII]
3/157/63Cirene [XXIII]
1/158/63Cirene [XXIII]
2/158/63Cirene [XXIII]
3/158/63Cirene [XXIII]
63/-/-/63Cirene [XXIII]
LXIII/-/63Cirene [XXIII]
AA/-/-/63Cirene [XXIII]
5Coastal/Bardia Garrison
Frontier Guards 30 Group
17AABn/Bardia Garrison
Frontier Guards 30Group 
XLI/-/1 23Marzo [XXIII]
201/-/1 23Marzo [XXIII]
1/-/-/1 23Marzo [XXIII]
LMC/-/219/1 23Marzo [XXIII]
LGB/-/219/1 23Marzo [XXIII]
LMC/-/233/1 23Marzo [XXIII]
1/201/1 23Marzo [XXIII]
2/201/1 23Marzo [XXIII]
3/201/1 23Marzo [XXIII]
AA/-/-/1 23Marzo [XXIII]
AA/-/-/1 23Marzo [XXIII]
114/219/1 23Marzo [XXIII]
118/219/1 23Marzo [XXIII]
119/219/1 23Marzo [XXIII]
129/233/1 23Marzo [XXIII]
133/233/1 23Marzo [XXIII]
148/233/1 23Marzo [XXIII]
8/2/Libya A.C.East
4/10/Libya A.C.East
2/13/Libya A.C.East
159/50/12/Libya A.C.East
LBG/-/233/1 23Marzo [XXIII]
HMS Kent
Mare Nostrum
Via Balbia
