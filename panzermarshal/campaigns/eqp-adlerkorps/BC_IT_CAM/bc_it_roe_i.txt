ROER TRIANGLE
January 14th, 1945

With the failed German offensive in the Ardennes, we now have an opportunity to push and chase them towards the Rhine.  There are two main barriers to our further advance - the river Maas running along the Dutch/Belgian border, and the river Rur running from the German Eifel area through Heinsberg towards Roermond - where it joins the Maas.  The front in this area has settled into what is being called 'Saefeller Beek' a triangular area protruding from the frontline, otherwise known as the 'Roer Triangle', and you have been tasked to clear it.