TACTICAL VICTORY

Good display, sir.  We've extended our beachhead a little further, but since then, we've made about as much ground as an asthmatic ant with a heavy load of shopping.  Montgomery is extremely grateful for the gains you've made, and sends his warmest regards - oh and a bottle of scotch, which I've already consumed.