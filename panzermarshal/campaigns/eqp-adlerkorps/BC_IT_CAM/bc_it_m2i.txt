MANDRITSARA
September 12th, 1942

Hostilities on the island have continued at a low level for several months now.  We have landed on the western coast of the island at Majunga and are now pressing south towards the capital Tananarive.  For this task we have called upon your unit to spearhead our advance through the mountains, and to capture the city.  Resistance is expected to be minimal at the worst and non-existent at the best.  We fully expect the Vichy forces on Madagascar to surrender soon.