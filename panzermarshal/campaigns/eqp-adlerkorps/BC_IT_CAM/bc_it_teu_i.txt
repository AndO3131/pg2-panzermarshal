WESERBERGLAND
April 6th, 1945

I'm afraid you're trip back home has been cancelled.  The Allied commander wants you to take your forces and establish a bridgehead over the Weser River, although you needn't worry too much about enemy resistance.  Intelligence reports suggest that the German units within the area are a mix of various under strength replacement units and local militia, which should be no match for your crack troops.