The Plattensee offensive - March 1945<br>

Hitler has grouped Ist and IInd SS Panzer Corps West of Budapest in an attempt to retake the city from the Russians, thereby destroying the Bolsheviks in the area between the Drava, the Danube and the Plattensee. Your forces are positioned on the left flank of the offensive.
An early thaw has caused severe flooding in the area, producing swamp-like conditions. On my way to your HQ, I saw 15 King Tigers that had sank into the mud up to their turrets. What is Hitler thinking having 4SS Panzer Divisions attack on this terrain?

