Counter Attack on the Mius - July 1943<br>
The bolsheviks have crossed the Mius South of the Kursk salient near the village of Stepanovka in an effort to divert forces from the attack on Kursk. Throw them back to the other side of the river.
LSSAH has departed for Italy. We got their armored vehicles, bringing our Panzer forces up to strength. 

