Panzer-Verbaende Ostpreussen Sep1939<br>

In July 1939, the OKH decided to create a unique unit in preparation for the assault on Poland. This unit was officially designated as Panzer-Verbaende Ostpreussen, better known as Panzer Division Kempf. The division was composed of both Heer and Waffen SS units.

The official explanation for moving the unit into Ostpreussen in July 1939 was that it would participate at the Tannenberg Celebration to commemorate Hindenberg's victory at the Tannenberg in WW1.

After negotiations between Germany and Poland broke down, Pz Division Kempf units crossed the border into Poland on August 26th 06h00. At the last minute the order to attack was postponed and it wasn't until 09h00 that all the units had been recalled to their assembly points. 

Negotiations to prevent hostilities between Poland and Germany fell through for a final time on August 30th and German units crossed the border a second time on September 1st 04h45.

