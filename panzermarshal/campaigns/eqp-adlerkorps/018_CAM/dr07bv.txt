Operation Marita April1941 BV
Congratulations! It was the Waffen SS that captured Belgrade and not Heer troops. One of your SS patrols entered the city center and has hoisted the Nazi flag atop the German legation. About two hours later the mayor of Belgrade officially handed over the city to Obersturmfuehrer Klingenberg who was accompanied by a representative of the German Foreign Ministry, previously interned by the Yugoslavs. 

PLAYER ORDERS:
- If playing with restrictions: You can add a Sturmgeschuetz Anti Tank unit, an artillery unit (no heavier than 105mm) and a Krad Infanterie unit to your core forces for the upcoming invasion of Russia.

