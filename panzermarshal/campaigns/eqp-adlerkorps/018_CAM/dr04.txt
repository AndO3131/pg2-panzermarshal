Crossing the Schelde May 1940<br>

Take the city of Antwerp and advance South West. There is no time for sightseeing, this is Blitzkrieg warfare! Opposite you are the remnants of the Belgian Army and the retreating French 7th Army that was rushing into The Netherlands only a few days ago. The Belgian King has ordered 2 infantry divisions to prevent you from crossing the Schelde river. Rumor is that a cavalry division is reorganizing behind the front. The bulk of the French forces comes from the 9th Mechanized Division. 

PLAYER ORDERS:
- Take all victory hexes.
- If playing with restrictions: You can deploy the following core units: 1FlaK+ 3TowedAty+ 3Inf+ 0Pio+ 0KradInf+ 1Recon.

