Another superb tactical performance, Herr General.  The British were reinforced and dug in, but you quickly overwhelmed them!  Now we must move rapidly to take El Alamein as preparation for our conquest of Egypt and the Suez Kanal.  You will be Feldmarschall Rommel's lead commander in this attack!

 